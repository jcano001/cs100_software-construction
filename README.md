# CS 100 - Software Construction
## UCR | Fall 2013
>Topics include design and implementation strategies; selection and mastery of programming languages, environment tools, and development processes. Develops skill in programming, testing, debugging, performance evaluation, component integration, maintenance, and documentation. Covers professional and ethical responsibilities and the need to stay current with technology. 
>All programs have been coded in either C or C++

## Homeworks
### hw1
> + linked-list rolodex 
> + stack container

### hw2
> + a bank program that uses Coin objects
> + `my_ls` command as a subset of the UNIX command `ls`

### hw3
> + String implemented as a character array
> + `proc` is a program that creates four processes using `fork()` with each process printing either 'A', 'B', 'C', or 'D' 10,000 times
> + `copy` command as a subset of the UNIX command `cp`

### hw4
> + String implemented as a character linked list
> + `handle_signals` is a program that handles SIGINT, SIGQUIT, and SIGSTOP UNIX signals
> + `my_shell` a simple UNIX shell emulation

### hw5
> + polymorphism demonstration
> + `my_shell` is a simple UNIX shell emulation. Handles redirection with > and <

### hw6
> + template Matrix as a template Array of template Arrays. Handles IndexOutOfBoundsException
>  + `my_shell` is a simple UNIX shell emulation. Handles piping with |

### hw7
> + `my_find` script that emulates UNIX `find`
> + `my_which` script that emulates UNIX `which`
> + `dircheck` script displays information about specified directory
> + `freq` counts the frequency of words in a given file, excluding a set of "exlusion words"
> + `sort_int` sorts and input stream of words and writes the even and odd numbers to evenNumbers.txt and oddNumbers.txt, respectively

### hw8
> + iterative Client/Server programs
> + concurrent Client/Server programs

### hw9
> + a multithreaded Client program that makes 10 simultaneous threaded connections to the concurrent Server
