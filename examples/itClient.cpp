/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #8
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

using namespace std;

// max number of bytes we can get at once
const int MAXDATASIZE = 300;
const int port = 45660;
 
int main( int argc, char *argv[] )
{
	int sockfd, numbytes;
	char buf[MAXDATASIZE];
	struct hostent *he;
	/* connector's address information */
	struct sockaddr_in their_addr;

	/* if no command line argument supplied */
	if(argc != 3)
	{
		cerr << "Client usage: itClient <host name> <dir>\n";
		exit(1);
	}


	/* get the host info */
	if((he=gethostbyname(argv[1])) == NULL)
	{
		cerr << "gethostbyname() error\n";
		exit(1);
	}
	else
		cerr << "Client-the remote host is: " << argv[1] << endl;
     
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		cerr << "socket() error\n";
		exit(1);
	}
	else
		cout << "Client-the socket() sockfd is OK...\n";
     
	/* host byte order */
	their_addr.sin_family = AF_INET;
	/* short, network byte order */
	cout << "Server-using " << argv[1] << " and port " << port << "...\n";
	their_addr.sin_port = htons(port);
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	/* zero the rest of the struct */
	memset(&(their_addr.sin_zero), '\0', 8);
   
	if(connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1)
	{
		cerr << "connect() error\n";
		exit(1);
	}
	else
		cout << "Client-the connect() is OK...\n";

	for( int i = 0; i < 5; i++ )
	{
		numbytes = read(sockfd,buf,MAXDATASIZE-1);
		//numbytes = recv(sockfd, buf, MAXDATASIZE-1, 0);
		if (numbytes == -1)    
		{
			cerr << "read() error\n";
			exit(1);
		}
		else
		{
			cout << "Client-the read() is OK...\n";
			buf[numbytes] = '\0';
			cout << "Client-received " << numbytes << ": " << buf << endl;
		}
		sleep(1);
	}

	cout << "Client-closing sockfd\n";
	close(sockfd);
	cout << "Client exiting.\n";

	return 0;
}
