/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #8
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
 
using namespace std;

const int port = 45660;
const int MAXDATASIZE = 300;

/* how many pending connections queue will hold */
const int BACKLOG = 10;
 
int main( int argc, char *argv[] )
{
	/* listen on sock_fd, new connection on new_fd */
	int sockfd, new_fd;
	/* my address information, address where I run this program */
	struct sockaddr_in my_addr;
	/* remote address information */
	struct sockaddr_in their_addr;
	int sin_size;

	if( (sockfd = socket(AF_INET,SOCK_STREAM,0)) == -1)
	{
		cerr << "socket() error\n";
		exit(1);
	}
	else
		cout << "socket() is OK...\n";

	/* host byte order */
	my_addr.sin_family = AF_INET;
	/* short, network byte order */
	my_addr.sin_port = htons(port);
	/* auto-fill with my IP */
	my_addr.sin_addr.s_addr = INADDR_ANY;

	/* zero the rest of the struct */
	memset(&(my_addr.sin_zero), 0, 8);
     
	if(bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1)
	{
		cerr << "bind() error\n";
		exit(1);
	}
	else
		cout << "bind() is OK...\n";
     
	if(listen(sockfd, BACKLOG) == -1)
	{
		cerr << "listen() error\n";
		exit(1);
	}
	else
		cout << "listen() is OK...\n";
     
	/* ...other codes to read the received data... */
 
	sin_size = sizeof(struct sockaddr_in);

	cout << "going to block via accept()...\n";
	if( (new_fd = accept(sockfd, (struct sockaddr *)&their_addr, (socklen_t *)&sin_size)) == -1 )
		cerr << "accept() error\n";
	else
		cout << "accept() is OK...\n";
     
	char *msg[5] = {
		"This is the server, hello!",
		"Going once",
		"Going twice",
		"Going thrice",
		"This is the server, good bye!"};

	for( int i = 0; i < 5; i++ )
	{
		if(write(new_fd,msg[i], strlen(msg[i])) == -1)
		{
			cerr << "write() error\n";
			exit(1);
		}
		else
			cout << "write() is OK...\n";
		sleep(1);
	}
	

	/*.....other code.......*/
     
	close(new_fd);
	close(sockfd);
	cout << "Server exiting.\n";

	return 0;
}
