/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #8
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <fstream>
#include <sys/file.h>

using namespace std;

// max number of bytes we can get at once
const int MAXDATASIZE = 50000;
const int port = 45660;
const int FILENAMESIZE = 256;
 
int main( int argc, char *argv[] )
{
	int clientfd, numbytes;
	char buf[MAXDATASIZE];
	struct hostent *he;
	/* connector's address information */
	struct sockaddr_in server_addr;

	/* if no command line argument supplied */
	if(argc < 3)
	{
		cerr << "Client usage: itClient <host name> <dir>\n";
		exit(1);
	}

	/* directory listing */
	char* dir = argv[2];
	int num_dirs = argc-2;
	char* dir_list[num_dirs];
	for( int i = 0; i < argc; i++ )
		dir_list[i] = argv[i+2];

	/* get the host info */
	if((he=gethostbyname(argv[1])) == NULL)
	{
		cerr << "gethostbyname() error\n";
		exit(1);
	}
	else
		cerr << "Client-the remote host is: " << argv[1] << endl;
     
	if((clientfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		cerr << "Client-socket() error\n";
		exit(1);
	}
	else
		cout << "Client-the socket() clientfd is OK...\n";
     
	/* host byte order */
	server_addr.sin_family = AF_INET;
	/* short, network byte order */
	cout << "Server-using " << argv[1] << " and port " << port << "...\n";
	server_addr.sin_port = htons(port);
	server_addr.sin_addr = *((struct in_addr *)he->h_addr);

	/* connect client to server */
	if(connect(clientfd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
	{
		cerr << "Client-connect() error\n";
		exit(1);
	}
	else
		cout << "Client-the connect() is OK...\n";

	/* zero the rest of the struct */
	memset(&(server_addr.sin_zero), '\0', 8);

	/* client sends request for directory listing */
	if( write(clientfd,dir,strlen(dir)) == -1 )
	{
		cerr << "Client-write() error\n";
		exit(1);
	}
	else
	{
		cout << "Client-write() is OK...\n";
		cout << "Client-requesting " << dir << "...\n";
	}
	sleep(1);
	
	/* client reads server reply */
	/* server first sends file name */
	char fileName[FILENAMESIZE];
	char path[FILENAMESIZE];
	int fd;

	strcpy(path,"./");
	strcat(path,dir);
	if( (fd = mkdir(path,0777)) == -1 )
		cout << "mkdir() failed\n";

	numbytes = 0;
	bzero(path,FILENAMESIZE);
	int n = 0;
	FILE* file;
	while( (numbytes = read(clientfd,fileName,FILENAMESIZE-1)) > 0 )
	{

		fileName[numbytes] = '\0';
		strcpy(path,".");
		strcat(path,fileName);

		/* first, create and open file */
		if( (fd=open(path,O_WRONLY | O_CREAT,0555)) == -1 )
		{
			if( path == "." )
				cout << "failed to transfer file " << path << endl;
			continue;
		}
		sleep(1);
		cout << path << endl;
		if( (numbytes=read(clientfd,buf,MAXDATASIZE-1)) == -1 )
			cout << "Client-read() file failed\n";
		if( (numbytes=write(fd,buf,sizeof(buf))) == -1 )
			cout << "Client-write() file failed\n";
		


		bzero(fileName,FILENAMESIZE);
		bzero(path,FILENAMESIZE);
		bzero(buf,MAXDATASIZE);
		sleep(1);
	}
	if( numbytes == -1 )
	{
		cerr << "Client-read() error\n";
		exit(1);
	}

	cout << "Client-closing clientfd\n";
	close(clientfd);
	cout << "Client exiting.\n";

	return 0;
}
