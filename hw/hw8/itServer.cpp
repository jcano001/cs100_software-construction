/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #8
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
 
#include <sys/wait.h>
//#include <fstream>
#include <sys/file.h>
using namespace std;

const int port = 45660;
const int MAXDATASIZE = 256;

/* how many pending connections queue will hold */
const int BACKLOG = 10;
 
int main( int argc, char *argv[] )
{
	/* listen on serverfd, new connection on clientfd */
	int serverfd, clientfd;
	/* number of bytes to be received */
	int numbytes = 0;
	/* my address information, address where I run this program */
	struct sockaddr_in server_addr;
	/* remote address information */
	struct sockaddr_in client_addr;
	int sin_size;
	char dir[MAXDATASIZE];
	/* fork variables */
	int status;
	int pid;

	if( (serverfd = socket(AF_INET,SOCK_STREAM,0)) == -1)
	{
		cerr << "Server-socket() error\n";
		exit(1);
	}
	else
		cout << "Server-socket() is OK...\n";

	/* host byte order */
	server_addr.sin_family = AF_INET;
	/* short, network byte order */
	server_addr.sin_port = htons(port);
	/* auto-fill with my IP */
	server_addr.sin_addr.s_addr = INADDR_ANY;

	if(bind(serverfd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
	{
		cerr << "Server-[ERRNO " << errno << "] socket(): " << strerror(errno) << endl;
		exit(1);
	}
	else
		cout << "Server-bind() is OK...\n";
     
	if(listen(serverfd, BACKLOG) == -1)
	{
		cerr << "Server-listen() error\n";
		exit(1);
	}
	else
		cout << "Server-listen() is OK...\n";
     
	/* ...other codes to read the received data... */
 
	sin_size = sizeof(struct sockaddr_in);

	cout << "Server going to block via accept()...\n";
	if( (clientfd = accept(serverfd, (struct sockaddr *)&client_addr, (socklen_t *)&sin_size)) == -1 )
		cerr << "Server-accept() error\n";
	else
		cout << "Server-accept() is OK...\n";


	/* first, read how many requests the client is making */
	int num_dirs;
	numbytes = read(clientfd,&num_dirs,MAXDATASIZE-1);

	/* server reads client request for directory listing */
	for( int i = 0; i < num_dirs; i++ )
	{
		/* zero the rest of the struct */
		memset(&(server_addr.sin_zero), 0, 8);
     
		numbytes = read(clientfd,dir,MAXDATASIZE-1);
		if( numbytes < 0 )
		{
			cout << "Server-read() error\n";
			break;
		}
		dir[numbytes] = '\0';

		/* set up command to be executed */
		char* args[4] = {
			"ls",
			"-l",
			dir,
			NULL };

		/* fork */
		switch( pid = fork() ) {
			case -1:
				cout << "ERROR: fork failed\n";
				exit(1);
			case 0: // child 
				dup2(clientfd,STDOUT_FILENO); // STDOUT_FILENO = 1
				cout << "Directory listing for " << dir << ":\n";
				if( execvp( args[0], args ) == -1 )
					perror("execvp failed");
				exit(1);
			default: // parent 
				while( wait(&status) != pid );
				break;
		}
		numbytes = 0;
	}


     
	close(clientfd);
	close(serverfd);
	cout << "Server exiting.\n";

	return 0;
}
