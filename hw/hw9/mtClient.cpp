/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #9
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <fstream>
#include <sys/file.h>
#include <pthread.h>

using namespace std;

// max number of bytes we can get at once
const int MAXDATASIZE = 50000;
const int port = 45660;
const int FILENAMESIZE = 256;
const int NUM_THREADS = 10;

pthread_mutex_t print_mutex;

struct arg_struct {
	int threadid;
	int clientfd;
	char* dir;
	struct sockaddr_in server_addr;
	char* hostname;
};


/* directory names for output */
char* dir_list[NUM_THREADS] = {
	"Thread1files",
	"Thread2files",
	"Thread3files",
	"Thread4files",
	"Thread5files",
	"Thread6files",
	"Thread7files",
	"Thread8files",
	"Thread9files",
	"Thread10files" };


void *doThreadWork(void *arguments)
{
	struct arg_struct *args = (struct arg_struct *)arguments;
	int threadid = args->threadid;
	int clientfd;// = args->clientfd;
	char* dir = args->dir;
	struct sockaddr_in server_addr = args->server_addr;
	struct hostent *he;
	char* hostname = args->hostname;

	/* get the host info */
	if((he=gethostbyname(hostname)) == NULL)
	{
		cerr << "gethostbyname() error\n";
		exit(1);
	}
	else
		cerr << "Client-the remote host is: " << hostname << endl;
     
	if((clientfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		cerr << "Client-socket() error\n";
		exit(1);
	}
	else
		cout << "Client-the socket() clientfd is OK...\n";

	/* host byte order */
	server_addr.sin_family = AF_INET;
	/* short, network byte order */
	cout << "Server-using " << hostname << " and port " << port << "...\n";
	server_addr.sin_port = htons(port);
	server_addr.sin_addr = *((struct in_addr *)he->h_addr);

	int  numbytes;
	char buf[MAXDATASIZE];

	sleep(1);

	/* connect client to server */
	if(connect(clientfd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
	{
		cerr << "Client " << threadid << "-connect() error\n";
		exit(1);
	}
	else
		cout << "Client " << threadid << "-the connect() is OK...\n";

	/* zero the rest of the struct */
	memset(&(server_addr.sin_zero), '\0', 8);

	cout << "Client " << threadid << "-requesting " << dir << "...\n";

	/* client sends request for directory listing */
	if( write(clientfd,dir,strlen(dir)) == -1 )
	{
		cerr << "Client " << threadid << "-write() error\n";
		exit(1);
	}
	else
	{
		cout << "Client " << threadid << "-write() is OK...\n";
		cout << "Client " << threadid << "-requesting " << dir << "...\n";
	}
	sleep(1);

	/* client reads server reply */
	/* server first sends file name */
	char fileName[FILENAMESIZE];
	char path[FILENAMESIZE];
	int fd;

	strcpy(path,"./");
	strcat(path,dir_list[threadid]);
	if( (fd = mkdir(path,0777)) == -1 )
		cout << "mkdir() " << path << " failed\n";

	numbytes = 0;
	bzero(path,FILENAMESIZE);
	int n = 0;
	FILE* file;
	while( (numbytes = read(clientfd,fileName,FILENAMESIZE-1)) > 0 )
	{

		fileName[numbytes] = '\0';
		strcpy(path,"./");
		strcat(path,dir_list[threadid]);
		strcat(path,"/");
		strcat(path,fileName);

		/* first, create and open file */
		if( (fd=open(path,O_WRONLY | O_CREAT,0555)) == -1 )
		{
			if( path == "." )
				cout << "failed to transfer file " << path << endl;
			continue;
		}
		sleep(1);
		cout << path << endl;
		if( (numbytes=read(clientfd,buf,MAXDATASIZE-1)) == -1 )
			cout << "Client-read() file failed\n";
		if( (numbytes=write(fd,buf,sizeof(buf))) == -1 )
			cout << "Client-write() file failed\n";
		


		bzero(fileName,FILENAMESIZE);
		bzero(path,FILENAMESIZE);
		bzero(buf,MAXDATASIZE);
		sleep(1);
	}
	if( numbytes == -1 )
	{
		cerr << "Client-read() error\n";
		exit(1);
	}

	cout << "Client " << threadid << "-closing clientfd\n";
	close(clientfd);
	cout << "Client " << threadid << " exiting.\n";
	
}

 
int main( int argc, char *argv[] )
{
	int clientfd, numbytes;
	char buf[MAXDATASIZE];
	struct hostent *he;
	/* connector's address information */
	struct sockaddr_in server_addr;
	char* hostname = argv[1];

	/* if no command line argument supplied */
	if(argc < 3)
	{
		cerr << "Client usage: itClient <host name> <dir>\n";
		exit(1);
	}

	/* directory listing */
	char* dir = argv[2];


	pthread_t threads[NUM_THREADS];
	struct arg_struct args[NUM_THREADS];

	pthread_mutex_lock(&print_mutex);
	for( long t = 0; t < NUM_THREADS; t++ )
	{
		args[t].threadid = t;
		args[t].clientfd = clientfd;
		args[t].dir = dir;
		args[t].server_addr = server_addr;
		args[t].hostname = hostname;

		cout << "Creating thread " << t << endl;
		if( int rc = pthread_create(&threads[t],NULL,doThreadWork,(void *)&args[t]) )
		{
			cerr << "ERROR: return code from pthread_create() is " << rc << endl;
			exit(-1);
		}
		sleep(3);
	}
	for( long t = 0; t < NUM_THREADS; t++ )
	{
		pthread_join(threads[t],NULL);
	}
	pthread_mutex_unlock(&print_mutex);

	return 0;
}
