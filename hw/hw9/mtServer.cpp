/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #9
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
//#include <cstdio.h> 
#include <sys/wait.h>
#include <sys/file.h>
#include <sys/sendfile.h>

using namespace std;

const int port = 45660;
const int MAXDATASIZE = 50000;
const int FILENAMESIZE = 256;

/* how many pending connections queue will hold */
const int BACKLOG = 10;
 
int main( int argc, char *argv[] )
{
	/* listen on serverfd, new connection on clientfd */
	int serverfd, clientfd;
	/* number of bytes to be received */
	int numbytes = 0;
	/* my address information, address where I run this program */
	struct sockaddr_in server_addr;
	/* remote address information */
	struct sockaddr_in client_addr;
	int sin_size;
	char buf[MAXDATASIZE];
	char dir[FILENAMESIZE];
	char fullPath[FILENAMESIZE];
	/* fork variables */
	int status;
	int pid;

	if( (serverfd = socket(AF_INET,SOCK_STREAM,0)) == -1)
	{
		cerr << "Server-socket() error\n";
		exit(1);
	}
	else
		cout << "Server-socket() is OK...\n";

	/* host byte order */
	server_addr.sin_family = AF_INET;
	/* short, network byte order */
	server_addr.sin_port = htons(port);
	/* auto-fill with my IP */
	server_addr.sin_addr.s_addr = INADDR_ANY;

	if(bind(serverfd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
	{
		cerr << "Server-[ERRNO " << errno << "] socket(): " << strerror(errno) << endl;
		exit(1);
	}
	else
		cout << "Server-bind() is OK...\n";
     
	if(listen(serverfd, BACKLOG) == -1)
	{
		cerr << "Server-listen() error\n";
		exit(1);
	}
	else
		cout << "Server-listen() is OK...\n";
     
	/* ...other codes to read the received data... */
 
	sin_size = sizeof(struct sockaddr_in);



while( 1 )
	{
		//accept
		//fork
			//child: close serverfd
				// process request
				// close clientfd
			//parent: close clientfd
		cout << "Server going to block via accept()...\n";
		if( (clientfd = accept(serverfd, (struct sockaddr *)&client_addr, (socklen_t *)&sin_size)) == -1 )
			cerr << "Server-accept() error\n";
		else
			cout << "Server-accept() is OK...\n";

		/* fork */
		switch( pid = fork() ) {
			case -1:
				cout << "ERROR: fork failed\n";
				exit(1);
			case 0: // child 
				close(serverfd);

				/*****************************************************************/
				/*                                                               */
				/*                    PROCESS CLIENT REQUEST                     */
				/*                                                               */
				/*****************************************************************/
				/* zero the rest of the struct */
				memset(&(server_addr.sin_zero), 0, 8);

				/* first, server reads client request for directory listing */
				numbytes = read(clientfd,dir,MAXDATASIZE-1);
				if( numbytes < 0 )
				{
					cout << "Server-read() error\n";
					exit(1);
				}
				dir[numbytes] = '\0';
				cout << "Server-received request for " << dir << "...\n";

				/* open directory */
				DIR* dirp;
				if( !(dirp = opendir(dir)) )
				{
					cerr << "Error(" << errno << ") opening " << dir << endl;
					exit(1);
				}
 
				// valid directory
				dirent* direntp;

				int fd;
				FILE* file;
				/* iterate thru all files in directory */
				while( (direntp = readdir(dirp)) ) {
					char* fileName = direntp->d_name;
					strcpy(fullPath,dir);
					strcat(fullPath,"/");
					strcat(fullPath,direntp->d_name);
				/* if file is a regular file, start sending to client */
				if( direntp->d_type == DT_REG )
				{
					/* send the name of the file so that the client may create it */
					if( (write(clientfd,fileName,strlen(fileName))) == -1 )
						cout << "Server-write() failed\n";
					if( (file = fopen(fullPath,"r")) == NULL )
					{
						cout << "unable to open file " << fullPath << endl;
						bzero(buf,MAXDATASIZE);
					}
					else
					{
						if( (numbytes=fread(buf,sizeof(char),MAXDATASIZE-1,file)) == -1 )
							cout << "Server-fread() failed\n";
					}
					sleep(1);
					if( (write(clientfd,buf,sizeof(buf))) == -1 )
						cout << "Server-write() failed\n";
						sleep(1);
					}
					numbytes = 0;
					bzero(fullPath,FILENAMESIZE);
					bzero(buf,MAXDATASIZE);
					sleep(1);
				}

				close(clientfd);
				exit(0);
			default: // parent 
				//while( wait(&status) != pid );
				close(clientfd);
				break;
		}

	}



     
	close(clientfd);
	close(serverfd);
	cout << "Server exiting.\n";

	return 0;
}
