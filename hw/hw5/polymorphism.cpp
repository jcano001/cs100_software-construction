/* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #5
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <string>

using namespace std;

#define BASE_STAT 100
#define BASE_HP 500

/* Base class Hero initializes all attributes to 100 */
/* Each derived class's data member will be a factor of the Base class's */
class Hero
{
public:
	Hero()
	{
		level = 1;
		hp = BASE_HP;
		strength = BASE_STAT;
		mana = BASE_STAT;
		intelligence = BASE_STAT;
		agility = BASE_STAT;
		speed = BASE_STAT;
		baseAttack = BASE_STAT;
	}

	virtual ~Hero() {}

	virtual string getType() {}

	int getLevel()
	{
		return level;
	}

	/* Pure virtual function specialAttack() calculates the damage of a hero's unique special attack */
	virtual double specialAttack() = 0;

	/* Virtual function to level up hero; can be called on any Hero type */
	virtual void levelUp()
	{
		// Increases level and stats based on level
		// Factors for Base Class Hero are 1
		level++;
		hp = BASE_HP*level;
		strength = BASE_STAT*level;
		mana = BASE_STAT*level;
		intelligence = BASE_STAT*level;
		agility = BASE_STAT*level;
		speed = BASE_STAT*level;
		baseAttack = BASE_STAT*level;
	}

protected:
	double hp;
	double strength;
	double mana;
	double intelligence;
	double agility;
	double speed;
	double baseAttack;
	int level;
};

/* Derived class Warrior is a Strength-Heavy Hero */
class Warrior : public Hero
{
public:
	Warrior()
	{
		// Factors are arbitrary
		hp = BASE_HP * 1.2;
		strength = BASE_STAT * 1.6;
		mana = BASE_STAT * 0.6;
		intelligence = BASE_STAT * 0.76;
		agility = BASE_STAT * 0.82;
		speed = BASE_STAT * 0.96;
		baseAttack = BASE_STAT;
		type = "Warrior";
	}

	virtual string getType()
	{
		return type;
	}

	// Made up formula to calculate damage of specialAttack
	virtual double specialAttack()
	{
		double damage = (level*baseAttack)+((speed/25)*strength);
		damage = damage/((strength/agility)+(strength/intelligence));
		damage = damage/10;
		return damage;
	}
	
	virtual void levelUp()
	{
		// Increases level and stats based on level
		// Factors for Derived Classed are arbitrary
		level++;
		hp = BASE_HP*(level*1.2);
		strength = BASE_STAT*(level* 1.2);
		mana = BASE_STAT*(level*0.87);
		intelligence = BASE_STAT*(level*0.92);
		agility = BASE_STAT*(level*0.87);
		speed = BASE_STAT*(level*0.96);
		baseAttack = BASE_STAT*level;
	}

protected:
	string type;
};

/* Derived class Wizard is a Intelligence-Heavy Hero */
class Wizard : public Hero
{
public:
	Wizard()
	{
		// Factors are arbitrary
		hp = BASE_HP * 0.8;
		strength = BASE_STAT * 0.67;
		mana = BASE_STAT * 2;
		intelligence = BASE_STAT * 1.6;
		agility = BASE_STAT * 0.85;
		speed = BASE_STAT * 0.97;
		baseAttack = BASE_STAT * 0.89;
		type = "Wizard";
	}

	virtual string getType()
	{
		return type;
	}

	// Made up formula to calculate damage of specialAttack
	virtual double specialAttack()
	{
		double damage = (level*baseAttack)+((speed/25)*intelligence);
		damage = damage/((intelligence/strength)+(intelligence/agility));
		damage = damage/10;
		return damage;
	}

	virtual void levelUp()
	{
		// Increases level and stats based on level
		// Factors for Derived Classed are arbitrary
		level++;
		hp = BASE_HP*(level*0.93);
		strength = BASE_STAT*(level*0.85);
		mana = BASE_STAT*(level*1.2);
		intelligence = BASE_STAT*(level*1.2);
		agility = BASE_STAT*(level*0.87);
		speed = BASE_STAT*(level*0.97);
		baseAttack = BASE_STAT*level;
	}

protected:
	string type;
};

/* Derived class Assassin is an Agility-Heavy Hero */
class Assassin : public Hero
{
public:
	Assassin()
	{
		// Factors are arbitrary
		hp = BASE_HP * 0.9;
		strength = BASE_STAT * 0.67;
		mana = BASE_STAT;
		intelligence = BASE_STAT * 0.72;
		agility = BASE_STAT * 1.07;
		speed = BASE_STAT * 0.98;
		baseAttack = BASE_STAT * 0.75;
		type = "Assassin";
	}

	virtual string getType()
	{
		return type;
	}

	// Made up formula to calculate damage of specialAttack
	virtual double specialAttack()
	{
		double damage = (level*baseAttack)+((speed/25)*agility);
		damage = damage/((agility/strength)+(agility/intelligence));
		damage = damage/10;
		return damage;
	}

	virtual void levelUp()
	{
		// Increases level and stats based on level
		// Factors for Derived Classed are arbitrary
		level++;
		hp = BASE_HP*(level*0.98);
		strength = BASE_STAT*(level*0.86);
		mana = BASE_STAT*(level*0.92);
		intelligence = BASE_STAT*(level*0.83);
		agility = BASE_STAT*(level*1.2);
		speed = BASE_STAT*(level*1.03);
		baseAttack = BASE_STAT*level;
	}

protected:
	string type;
};

/* Polymorphic function to level up all heroes listed in heroes[] by one (1) level */
void levelUpAll( Hero* heroes[], int n )
{
	for( int i = 0 ; i < n; i++ )
	{
		cout << heroes[i]->getType() << " leveling up from level " << heroes[i]->getLevel();
		heroes[i]->levelUp();
		cout << " to level " << heroes[i]->getLevel() << "!\n";
	}
	cout << endl;
}

/* Polymorphic function to make all heroes listed in heroes[] to use their special attack */
void attackAll( Hero* heroes[], int n )
{
	for( int i = 0; i < n; i++ )
	{
		cout << "Level " << heroes[i]->getLevel() << " " << heroes[i]->getType() << " uses a Special Attack!\n";
		cout << "DMG: " << heroes[i]->specialAttack() << endl << endl;
	}
	cout << endl;
}


int main() {
	cout << "********************\n";

	Hero *heroes[] = { new Warrior, new Wizard, new Assassin };
	int size = sizeof heroes / sizeof *heroes;

	attackAll(heroes,size);

	levelUpAll(heroes,size);

	attackAll(heroes,size);

	levelUpAll(heroes,size);

	attackAll(heroes,size);

	return 0;
}
