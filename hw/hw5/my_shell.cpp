/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #5
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string>
#include <vector>
#include <fstream>
#include <sys/file.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/file.h>

using namespace std;

int parse( string buf, char* args[] ) {
	vector<string> tok;
	char* command = strtok( const_cast<char*>(buf.c_str()), " " );
	int i = 0;
	while( command != NULL ) {
		args[i] = command;
		i++;
		command = strtok( NULL, " " );
	}
	return i;
}

void removeFromArray( char* args[], int index ) {
	for( int i = index; args[i] != NULL; i++ ) 
		args[i] = args[i+1];
}

void printArray( char* args[] ) {
	for( int i = 0; args[i] != NULL; i++ ) 
		cout << args[i] << " ";
	cout << endl;
}


int execCommand( char* args[], int numArgs ){
	int pid;
	int status;
	
	char* newOutputFile = "";
	int newOut_fd = 0;
	char* existingInputFile = "";
	int existingIn_fd = 0;

	char* progName = args[0];
	args[numArgs] = NULL;

	bool bg = false;

	if( strcmp(progName,"cd") == 0 ) {
		const char* home = getenv("HOME");
		chdir(home ? args[1] : "." );
	}
	else if( strcmp(progName,"") != 0 ) {
		switch( pid = fork() ) {
			case -1:
				cout << "ERROF: fork failed\n";
				exit(1);
			case 0: // child 
				for( int i = 0; i < numArgs; i++ ) {
					char* arg = args[i];
					if( strcmp(arg,"<") == 0 ) {
						newOutputFile = args[i-1];
						existingInputFile = args[i+1];
						newOut_fd = open(newOutputFile, O_WRONLY | O_CREAT, 0666 );
						existingIn_fd = open(existingInputFile, O_RDONLY );	
						dup2(existingIn_fd,STDIN_FILENO); // STDIN_FILENO = 0
						args[i] = NULL;				
					//	removeFromArray(args,i);
						close(existingIn_fd);
						close(newOut_fd);
					//	i = 0;
					}
					if( strcmp(arg,">") == 0 ) {
						newOutputFile = args[i+1];
						existingInputFile = args[i-1];
						newOut_fd = open(newOutputFile, O_WRONLY | O_CREAT, 0666);
						existingIn_fd = open(existingInputFile, O_WRONLY | O_CREAT, 0666 );
						dup2(newOut_fd,STDOUT_FILENO); // STDOUT_FILENO = 1
						args[i] = NULL;
					//	removeFromArray(args,i);
						close(existingIn_fd);
						close(newOut_fd);
					//	i = 0;
					}
					if( strcmp(arg,"&") == 0 ) {
						bg = true;
						args[i] = NULL;
					}
				}
				if( execvp( progName, args ) == -1 )
					perror("execvp failed");
				exit(1);
			default: // parent 
				if( !bg )
					while( wait(&status) != pid );
				break;
		}
		return 0;
	}
	return 0;
}

int execPipeCommand( const string &temp ) {
	int pipefd[2];
	int pid;
	int status;
	
	char* newOutputFile = "";
	int newOut_fd = 0;
	char* existingInputFile = "";
	int existingIn_fd = 0;

	//args[numArgs] = NULL;

	bool bg = false;
	
	char* childArgs[64];
	char* parentArgs[64];

	int index = temp.find("|");
	string lhs;
	lhs = temp.substr(0,index-1);
	string rhs = temp.substr(index+2);

	int parentNumArgs = parse(lhs,parentArgs);
	int childNumArgs = parse(rhs,childArgs);
	
	char* childProgName = childArgs[0];
	char* parentProgName = parentArgs[0];

	// make a pipe
	pipe(pipefd);

	switch(	pid = fork() ) {
		case -1: 
			cout << "ERROF: fork failed\n";
			exit(1);
		case 0: /* Child handles right side of pipe: reads from pipe */
			// close unused write end of pipe
			close(pipefd[1]);
			
			// replace standard output with output part of pipe
			dup2(pipefd[0],STDIN_FILENO);

			// execute
			//execCommand(childArgs,childNumArgs);
			//close(pipefd[0]);
			//exit(0);
			for( int i = 0; i < childNumArgs; i++ ) {
				char* arg = childArgs[i];
				if( strcmp(arg,"<") == 0 ) {
					newOutputFile = childArgs[i-1];
					existingInputFile = childArgs[i+1];
					newOut_fd = open(newOutputFile, O_WRONLY | O_CREAT, 0666 );
					existingIn_fd = open(existingInputFile, O_RDONLY );	
					dup2(existingIn_fd,STDIN_FILENO); // STDIN_FILENO = 0
					childArgs[i] = NULL;				
				//	removeFromArray(args,i);
					close(existingIn_fd);
					close(newOut_fd);
				//	i = 0;
				}
				if( strcmp(arg,">") == 0 ) {
					newOutputFile = childArgs[i+1];
					existingInputFile = childArgs[i-1];
					newOut_fd = open(newOutputFile, O_WRONLY | O_CREAT, 0666);
					existingIn_fd = open(existingInputFile, O_WRONLY | O_CREAT, 0666 );
					dup2(newOut_fd,STDOUT_FILENO); // STDOUT_FILENO = 1
					childArgs[i] = NULL;
				//	removeFromArray(args,i);
					close(existingIn_fd);
					close(newOut_fd);
				//	i = 0;
				}
				if( strcmp(arg,"&") == 0 ) {
					bg = true;
					childArgs[i] = NULL;
				}
			}
			if( execvp( childProgName, childArgs ) == -1 )
				perror("execvp failed");
			exit(0);
			break;
		default: /* Parent handles left side of pipe: writes to pipe */
			// close unused half of pipe
			close(pipefd[0]);

			// replace standard input with input part of pipe
			//dup2(pipefd[0],STDIN_FILENO);
			dup2(pipefd[1],STDOUT_FILENO);
			close(pipefd[1]);

			// execute
			//execCommand(parentArgs,parentNumArgs);
			for( int i = 0; i < parentNumArgs; i++ ) {
				char* arg = parentArgs[i];
				if( strcmp(arg,"<") == 0 ) {
					newOutputFile = parentArgs[i-1];
					existingInputFile = parentArgs[i+1];
					newOut_fd = open(newOutputFile, O_WRONLY | O_CREAT, 0666 );
					existingIn_fd = open(existingInputFile, O_RDONLY );	
					dup2(existingIn_fd,STDIN_FILENO); // STDIN_FILENO = 0
					parentArgs[i] = NULL;				
				//	removeFromArray(args,i);
					close(existingIn_fd);
					close(newOut_fd);
				//	i = 0;
				}
				if( strcmp(arg,">") == 0 ) {
					newOutputFile = parentArgs[i+1];
					existingInputFile = parentArgs[i-1];
					newOut_fd = open(newOutputFile, O_WRONLY | O_CREAT, 0666);
					existingIn_fd = open(existingInputFile, O_WRONLY | O_CREAT, 0666 );
					dup2(newOut_fd,STDOUT_FILENO); // STDOUT_FILENO = 1
					childArgs[i] = NULL;
				//	removeFromArray(args,i);
					close(existingIn_fd);
					close(newOut_fd);
				//	i = 0;
				}
			}
			if( execvp( parentProgName, parentArgs ) == -1 )
				perror("execvp failed");
			while( wait(&status) != pid );
			break;
	}
//	while( wait(&status) != pid );
	
	return 0;
}

int main( int argc, char* argv[] ){
	
	vector<string> command;
	string prompt = "% ";
	while( 1 ) {
		char* args[64];
		cout << flush << prompt << flush;
		string temp;
		getline(cin, temp);

		if( temp == "exit" ) exit(0);
		if( temp == "" ) getline(cin,temp);

		int found = temp.find("|");

		if( found >= 0 ) {
			execPipeCommand(temp);
		}
		else {
			int numArgs = parse(temp,args);
			execCommand(args,numArgs);
		}
	}
	//cout << "Exiting" << endl;
	return 0;
}

