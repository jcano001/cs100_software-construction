/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #4
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
#include "String.h"

const int STRING_LENGTH = 128;

String::String( const char* s ) {
	head = NULL;	
	if( s != "" ) {
		int i = 0;
		while( s[i] != '\0' ) {
			head->attach( s[i], head );
			i++;
		}
	}
}

String::String( const String &s ) {
	ListNode* temp = s.head;
	head = NULL;
	while( temp != NULL ) {
		head->attach( temp->info, head );
		temp = temp->next;
	}
}

String String::operator = ( const String & s ) {
	ListNode* temp = s.head;
	head = NULL;
	while( temp != NULL ) {
		head->attach( temp->info, head );
		temp = temp->next;
	}
	return *this;
}

char & String::operator [] ( const int index ) {
	char ret;
	if( inBounds(index) ) {
		ListNode* temp = head;
		for( int i = 0; i < index; i++ )
			temp = temp->next;
		ret = temp->info;
		return ret;
	}
	cout << "error: index out of bounds\n";
	exit(1);
}

int String::length() const {
	int count = 0;
	for( ListNode* p = head; p != NULL; ) {
		count++;
		p = p->next;
	}
	return count;
}

int String::indexOf( char c ) const {
	int count = 0;
	for( ListNode* p = head; p->info != c && p != NULL; ) {
		count++;
		p = p->next;
		if( p == NULL ) {
			cout << "error -1: no index for " << c << endl;
			return -1;
		}
	}
	return count;
}

bool String::operator == ( const String & s ) const {
	if( this->length() != s.length() )
		return false;

	ListNode* l = head;
	ListNode* r = s.head;
	while( l != NULL || r != NULL ) {
		if( l->info != r->info )
			return false;
		l = l->next;
		r = r->next;
	}

	return true;
}

/// concatenates this and s
String String::operator + ( const String & s ) {
	ListNode* temp = s.head;
	while( temp != NULL ) {
		head->attach( temp->info, head );
		temp = temp->next;
	}
	return *this;

}

/// concatenates s onto end of this
String String::operator += ( const String & s ) {
	ListNode* temp = head;
	head = NULL;
	while( temp != NULL ) {
		head->attach( temp->info, head );
		temp = temp->next;
	}
	temp = s.head;
	while( temp != NULL ) {
		head->attach( temp->info, head );
		temp = temp->next;
	}
	return *this;
}

void String::print( ostream & out ) {
	head->print( head );
}

void String::read( istream & in ) {
	char s[STRING_LENGTH];
	in.getline(s,STRING_LENGTH);
	
	String temp(s);
	*this = temp;
}

String::~String() {
	head->deleteList( head );
}

ostream & operator << ( ostream & out, String str ) {
	str.print(out);
	return out;
}
istream & operator >> ( istream & in, String & str ) {
	str.read(in);
	return in;
}

