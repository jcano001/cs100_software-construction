/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #4
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string>
#include <vector>
#include <fstream>
#include <sys/file.h>
#include <stdio.h>

using namespace std;

int readByBUF( const char* input, const char* output ) {
	int count = 0;
	char buf[BUFSIZ];
	int fd_in = open(input,O_RDONLY);
	int fd_out = open(output,O_WRONLY | O_CREAT );

	if( fd_in == -1 )
		perror(input);
	
	while( (count = read(fd_in,buf,BUFSIZ)) > 0 ) {
		write(fd_out,buf,count);	
	}

	close(fd_in);
	close(fd_out);

	return count;
}

int parse( string buf, char* args[] ) {
	vector<string> tok;
	char* command = strtok( const_cast<char*>(buf.c_str()), " " );
	int i = 0;
	while( command != NULL ) {
		args[i] = command;
		i++;
		command = strtok( NULL, " " );
	}
	return i;
}

int exec( char* args[], int numArgs ){
	int pid;
	int status;
	/*char progName[] = "/bin/";
	strcat( progName, args[0] );
	cout << progName << endl;*/
	char* progName = args[0];
	args[numArgs] = NULL;
	
	for( int i = 0; i < numArgs; i++ ) { 
		if( args[i] == "cd" ) {
			const char* home = getenv("HOME");
			chdir(home ? args[1] : "." );
		}
		else if( args[i] != "" ) {
			switch( pid = fork() ) {
				case -1:
					exit(1);
				case 0: // child 
					if( execvp( progName, args ) == -1 )
						perror("execvp failed");
					exit(1);
				default: // parent 
					while( wait(&status) != pid );
					break;
				}
			return 0;
		}
	}
//	while( wait(&status) != -1 ); //the parent process should wait for all of its children to terminate
}

#define BUFSIZE 1024

int main( int argc, char* argv[] ){
	
	vector<string> command;
	while( !cin.eof() ) {
		char* args[64];
		cout << "% ";
		string temp;
		getline(cin, temp);

		if( temp == "exit" ) exit(0);
	
		int numArgs = parse(temp,args);
		exec(args, numArgs);
	}
	cout << "Exiting" << endl;
	return 0;
}

