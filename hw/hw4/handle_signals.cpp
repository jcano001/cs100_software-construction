/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #4
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
#include <iostream>
#include <stdlib.h>
#include <signal.h>

using namespace std;

int INTERRUPT_COUNT;
int QUIT_COUNT;
int STOP_COUNT;

struct sigaction new_action, old_action;

void signal_handler( int signum ) {
	switch( signum ) {
		case SIGINT:
			cout << "I\n";
			INTERRUPT_COUNT++;
			break;
		case SIGQUIT:
			cout << "Q\n";
			QUIT_COUNT++;
			break;
		case SIGTSTP:
			cout << "S\n";
			STOP_COUNT++;
			if( STOP_COUNT < 3 ) {
				raise(SIGSTOP);
			}
			else {
				cout << "Interrupt: " << INTERRUPT_COUNT << endl;
				cout << "Stop: " << STOP_COUNT << endl;
				cout << "Quit: " << QUIT_COUNT << endl;
				exit(1);
			}
			break;
		default:
			break;
	}
}

void init_signal_handlers() {
	INTERRUPT_COUNT = 0;
	QUIT_COUNT = 0;
	STOP_COUNT = 0;
}

int main() {

	init_signal_handlers();
	
	
	new_action.sa_handler = signal_handler;
	sigemptyset( &new_action.sa_mask );
	new_action.sa_flags = 0;

	sigaction( SIGINT, NULL, &old_action );
	if( old_action.sa_handler != SIG_IGN )
		sigaction( SIGINT, &new_action, NULL );

	sigaction( SIGQUIT, NULL, &old_action );
	if( old_action.sa_handler != SIG_IGN )
		sigaction( SIGQUIT, &new_action, NULL );

	sigaction( SIGTSTP, NULL, &old_action );
	if( old_action.sa_handler != SIG_IGN )
		sigaction( SIGTSTP, &new_action, NULL );

	while(1) { cout << 'X'; }

	return 0;
}
