#include <iostream>
#include "String.h"

using namespace std;

int main() {

	cout << "Input for str1:\n";
	String str1;
	cin >> str1;
	
	cout << "Input for str2:\n";
	String str2;
	cin >> str2;

	cout << "str1: \"" << str1 << "\"\n"
		<< "str2: \"" << str2 << "\"\n";

	cout << endl;
	String str3 = str1;
	cout << "str3 = str1: \"" << str3 << "\"\n";

	cout << endl;
	str3 += str2;
	cout << "str3 += str2: \"" << str3 << "\"\n";

	cout << endl;
	cout << "str3 is of length " << str3.length() << endl;

	int index = -1;
	while( index < 0 || index >= str3.length() ) {
		cout << "Enter an index for str3: ";
		cin >> index;
	}
	cout << "str3[" << index << "]: '" << str3[index] << "'\n";

	cout << endl;
	cout << "Enter a char get the index in str3: ";
	char c;
	cin >> c;
	cout << "str3.indexOf( " << c << " ): " << str3.indexOf(c) << "\n";

	cout << endl;
	cout << "str1 == str2: ";
	if( str1 == str2 ) cout << "true\n";
	else cout << "false\n";

	cout << endl;
	str2 = str1;
	cout << "str2 = str1\nstr1: \"" << str1 << "\"\n"
		<< "str2: \"" << str2 << "\"\n";
	cout << "str1 == str2: ";
	if( str1 == str2 ) cout << "true\n";
	else cout << "false\n";

	cout << endl;
	str1 = str1 + str2;
	cout << "str1 = str1 + str2: \"" << str1 << "\"\n";

	return 0;
}
