# commands.txt
i. print all lines containing integer literals in the files *.cpp in the
current directory

ii. print all calls to `<<` to cout in the files *.h and *.cpp
	`grep --exclude='print_out.cpp' \"cout <<\" *.h *.cpp`

iii. delete all blank lines from a file called foo.cpp
	`sed '/^$/d' foo.cpp > temp | cat temp > foo.cpp | rm -f temp

iv. move all files anywhere below your home directory that have the extension `.o` or `.out` into a special directory called `.trash` contained in your home directory.
	`find ~ ! -path ~/.Trash\\* -name 'a.out' '*.o' -exec /bin/mv {} ~/.Trash \\;`

v. list the names of files in all directories named `bin` on the local machine
	`find /bin *`


vi. list the names of all fiels larger than 20 blocks in your account
directory
	`find ~ -size +20`

# String.h
>`String.cpp` is a linked list implementation of a string

# handle_signals
> `handle_signals.cpp` handles signals: ^C prints an I, ^\ prints a Q, ^Z prints an S and stops, ^Z three times outputs how many signals were handled then exits the program

# my_shell
> `my_shell.cpp` is my own implementation of a UNIX shell

#INSTALL
>The included `Makefile` compiles the programs on Linux machines using g++

`make all` or `make`
