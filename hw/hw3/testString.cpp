/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #3
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
#include <iostream>
#include "String.h"

using namespace std;

String foo( const String & s )
{
	String local(s);
	local[0] = 'M';
	return local;
}

int main() { 
	char* c = "Hello";
	String s(c);
	cout << "s: " << s << endl;
	cout << endl;

	cout << "foo:\n" << foo(s) << endl;

	String p = s;
	cout << "p: " << p << endl;

	cout << endl << "p[0]: " << p[0] << endl;
	
	String q = p.reverse();
	cout << "q: " << q << endl;

	cout << "Index of 'e' of q: " << q.indexOf('e') << endl;

	char* a = "World!";
	String t(a);
	
	if( s == p )
		cout << "true\n";
	if( t > p )
		cout << "T > P\n";
	if( t < p )
		cout << "T < P\n";

	String b = p + t;
	cout << "b: " << b << endl;

	s += "World!";
	cout << "s: " << s << endl;

	String u;
	cout << "Input string: ";
	cin >> u;
	cout << "String \"" << u << "\" created\n";




	return 0;
}
