/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #3
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
/// Both constructors should construct
/// this String from the parameter s
#include "String.h"

const int STRING_LENGTH = 128;

String::String( const char * s ) {
	len = 0;
	while( *(s+len) != '\0' )
		len++;
	buf = new char[STRING_LENGTH];
	
	for( int count = 0; count < len && *(s+count) != '\0'; count++ )
		buf[count] = *(s+count);
	buf[len] = '\0';
}

String::String( const String & s ) {
	len = s.len;
	buf = new char [STRING_LENGTH];
	for( int i = 0; i < len; i++ )
		buf[i] = s.buf[i];

	buf[len] = '\0';
}

String String::operator = ( const String & s ) {
	return String(s);
}
char & String::operator [] ( int index ) {
	return buf[index];
}
int String::size() {
	return len;
}

String String::reverse() { // does not modify this String
	char c[len];
	for( int i = len-1, count = 0; i >= 0; i--, count++ )
		c[count] = buf[i];
	return String(c);
}
int String::indexOf( char c ) {
	for( int i = 0; i < len; i++ ) {
		if( buf[i] == c )
			return i;
	}
	return -1;
}
int String::indexOf( String pattern ) {}
bool String::operator == ( String s ) {
	if( len != s.size() )
		return false;
	for( int i = 0; i < len; i++ ) {
		if( buf[i] != s[i] )
			return false;
	}
	return true;
}
bool String::operator != ( String s ) {
	if( len != s.size() )
		return true;
	for( int i = 0; i < len; i++ ) {
		if( buf[i] != s[i] )
		return true;
	}
	return false;
}
bool String::operator > ( String s ) {
	int this_val = 0;
	int s_val = 0;
	for( int i = 0; i < len; i++ )
		this_val += buf[i];
	for( int i = 0; i < s.size(); i++ )
		s_val += s[i];
	return this_val > s_val;
}
bool String::operator < ( String s ) {
	int this_val = 0;
	int s_val = 0;
	for( int i = 0; i < len; i++ )
		this_val += buf[i];
	for( int i = 0; i < s.size(); i++ )
		s_val += s[i];
	return this_val < s_val;
}
bool String::operator <= ( String s ) {
	int this_val = 0;
	int s_val = 0;
	for( int i = 0; i < len; i++ )
		this_val += buf[i];
	for( int i = 0; i < s.size(); i++ )
		s_val += s[i];
	return this_val <= s_val;
}
bool String::operator >= ( String s ) {
	int this_val = 0;
	int s_val = 0;
	for( int i = 0; i < len; i++ )
		this_val += buf[i];
	for( int i = 0; i < s.size(); i++ )
		s_val += s[i];
	return this_val >= s_val;
}
/// concatenates this and s to return result
String String::operator + ( String s ) {
	int totalSize = len + s.size();
	char c[totalSize];
	int count = 0;
	for( int i = 0; i < len; i++ ) {
		c[count] = buf[i];
		count++;
	}
	for( int i = 0; i < s.size(); i++ ) {
		c[count] = s[i];
		count++;
	}
	c[totalSize] = '\0';
	return String(c);
}
/// concatenates s onto end of this
String String::operator += ( String s ) {
	int totalSize = len + s.size();
	char c[totalSize];
	int count = 0;
	for( int i = 0; i < len; i++ ) {
		c[count] = buf[i];
		count++;
	}
	for( int i = 0; i < s.size(); i++ ) {
		c[count] = s[i];
		count++;
	}
	c[totalSize] = '\0';
	len = totalSize;
	for( int i = 0; i < totalSize; i++ ) {
		buf[i] = c[i];
	}
	buf[totalSize] = '\0';
	return String(c);
}
void String::print( ostream & out ) {
	int i = 0;
	while( inBounds(i) ) {
		cout << buf[i];
		i++;
	}
}
void String::read( istream & in ) {
	char s[STRING_LENGTH];
	in.getline(s,STRING_LENGTH);
	
	String temp(s);
	for( int i = 0; i < temp.len; i++ ) 
		this->buf[i] = temp.buf[i];
	this->len = temp.len;
	
}
String::~String() {
	delete [] buf;
}


ostream & operator << ( ostream & out, String str ) {
	str.print(out);
	return out;
}
istream & operator >> ( istream & in, String & str ) {
	str.read(in);
	return in;
}
