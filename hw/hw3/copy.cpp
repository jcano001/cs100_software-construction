/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #3
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sys/file.h>
#include <stdio.h>
#include "Timer.h"

using namespace std;

void myRead( const char* input, const char* output ) {
	ifstream inFile;
	ofstream outFile;

	inFile.open(input);
	outFile.open(output);

	char c;
	while( !inFile.eof() ) {
		c = inFile.get();
		outFile.put(c);
	}
	
}

int readByChar( const char* input, const char* output ) {
	int count = 0;
	char buf[1];
	int fd_in = open(input,O_RDONLY);
	int fd_out = open(output,O_WRONLY | O_CREAT | O_TRUNC,0666);
	
	while( (count = read(fd_in,buf,1)) > 0 ) {
		write(fd_out,buf,count);	
	}
	return count;
}

int readByBUF( const char* input, const char* output ) {
	int count = 0;
	char buf[BUFSIZ];
	int fd_in = open(input,O_RDONLY);
	int fd_out = open(output,O_WRONLY | O_CREAT | O_TRUNC,0666);
	
	while( (count = read(fd_in,buf,BUFSIZ)) > 0 ) {
		write(fd_out,buf,count);	
	}
	return count;
}


int main( int argc, char* argv[] ) {
	char* input;
	char* output;

	// if directory is specified
	if( argc > 2 ) {
		input = argv[1];
		output = argv[2];
	}
	else {
		cout << "Usage:\n" << "copy <input file> <outputfile>\n";
		return -1;
	}

	int n = 1;
	if( argc > 3 ) 
		n = static_cast<int>(*argv[3]);

	Timer t1,t2,t3;
	double wallTime1,wallTime2,wallTime3,sysTime1,sysTime2,sysTime3,userTime1,userTime2,userTime3;
	
	t1.start();
	for( int i = 0; i < n; i++ )
		myRead(input,output);
	t1.elapsedWallclockTime(wallTime1);
	t1.elapsedUserTime(userTime1);
	t1.elapsedSystemTime(sysTime1);
	cout << "myRead() function time:\n";
	cout << "Wallclock time: " << wallTime1 << "s" << endl
		<< "User time: " << userTime1 << "s" << endl
		<< "System time: " << sysTime1 << "s" << endl << endl;

	t2.start();
	for( int i = 0; i < n; i++ )
		readByChar(input,output);
	t2.elapsedWallclockTime(wallTime2);
	t2.elapsedUserTime(userTime2);
	t2.elapsedSystemTime(sysTime2);
	cout << "readByChar() function time:\n";
	cout << "Wallclock time: " << wallTime2 << "s" << endl
		<< "User time: " << userTime2 << "s" << endl
		<< "System time: " << sysTime2 << "s" << endl << endl;
	
	t3.start();
	for( int i = 0; i < n; i++ )
		readByBUF(input,output);
	t3.elapsedWallclockTime(wallTime3);
	t3.elapsedUserTime(userTime3);
	t3.elapsedSystemTime(sysTime3);
	cout << "readByBUF() function time:\n";
	cout << "Wallclock time: " << wallTime3 << "s" << endl
		<< "User time: " << userTime3 << "s" << endl
		<< "System time: " << sysTime3 << "s" << endl;

	return 0;
}
