# String.h
`String.h` is my own implementation of a String class library.
=====
`testString` is a test harness for my `String.h` library. It successfully tests all methods my String library.
=====
# `homer.txt` and `macros.txt`
=====
The text files `homer.txt` and `macros.txt` is a demonstration of my ability with the Vim text editor. The `typescript` shows the keystrokes used when modifying these text files.
=====
# proc
=====
`proc` is a program that creates four processes using `fork()`, with each process printing either 'A', 'B', 'C', or 'D' 10,000 times.
=====
# copy
=====
`copy` is similar to the UNIX command `cp`. The extra parameter and the end of the argument list determines the number of times the `read()` functions copy the source file to the destination file (default is 1).
=====
Usage:
`copy <source file> <destination file> <number of times to copy>`
=====
#INSTALL
The included `Makefile` compiles the programs on Linux machines using g++
=====
`make all` or `make`
