/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #3
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

void letter(char c) {
	for( int i = 0; i < 10000; i++ ) {
		cout << c;
		cout.flush();
	}
}

int main() {
	int status = 0;
	char letters[] = {'A','B','C','D','\0'};
	for(int i = 0; i < strlen(letters) && letters[i] != '\0'; i++) {
		pid_t pid = fork();
		if( pid == 0 ) {
			cout << "\nPID: " << getpid() << endl;
			letter(letters[i]);
			break;
		}
	}
	while(wait(NULL)>0);
	cout << endl;
	return 0;
}
