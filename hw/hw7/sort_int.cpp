/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #7
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

void printVector( vector<int> v )
{
	vector<int>::iterator i;
	for( i = v.begin(); i != v.end(); i++ )
		cout << *i << endl;
}

int main( int argc, char* argv[] )
{

/*	if( argc < 2 )
	{
		cout << "USAGE: freq.cpp <input file>\n";
		exit(1);
	}*/

	vector<int> numbers;

	cout << "Enter integers (CTRL+D to finish):\n";

	int input;
	while( cin >> input )
	{
		numbers.push_back(input);
	}

	char* oddName = "oddNumbers.txt";
	char* evenName = "evenNumbers.txt";

	ofstream oddFile(oddName);
	ofstream evenFile(evenName);
	if( !oddFile.is_open() )
	{
		cout << "Unable to open file " << oddName << endl;
		exit(1);
	}
	if( !evenFile.is_open() )
	{
		cout << "Unable to open file " << evenName << endl;
		exit(1);
	}

	sort(numbers.begin(),numbers.end());

	for( vector<int>::iterator i = numbers.begin(); i != numbers.end(); i++ )
	{
		if( *i % 2 == 0 ) // if even
		{
			evenFile << *i << " ";
		}
		else if( *i % 2 != 0 ) // if odd
		{
			oddFile << *i << " ";
		}
	}

	oddFile.close();
	evenFile.close();
	return 0;
}
