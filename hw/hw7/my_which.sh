#/*
#* Course: CS 100 Fall 2013
#*
#* First Name: Justin
#* Last Name: Cano
#* Username: canoj
#* email address: jcano001@ucr.edu
#*
#*
#* Assignment: Homework #7
#*
#* I hereby certify that the contents of this file represent
#* my own original individual work. Nowhere herein is there
#* code from any outside resources such as another individual,
#* a website, or publishings unless specifically designated as
#* permissible by the instructor or TA.*/ 
#! /bin/bash
path=(`echo $PATH | tr ":" " "`)
if [ $# -lt 1 ]
then
	echo "USAGE: bash my_script.sh <command>"
else
	for f in ${path[*]}
	do
		f=$f"/"$1
		if [ -x $f ]
		then
			echo $f
		fi
	done
fi
