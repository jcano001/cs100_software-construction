/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #7
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/ 
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <map>

using namespace std;


bool isExcluded( string word, vector<string> v )
{
	vector<string>::iterator i;
	for( i = v.begin(); i < v.end(); i++ )
	{
		if( word == *i )
			return true;
	}

	return false;
}

void printMap( map<string,int> m )
{
	map<string,int>::iterator i;
	for( i = m.begin(); i != m.end(); i++ )
	{
		cout << left << setw(10) << (i->first+": ") << setw(3) <<  i->second << endl;
	}
}

int main( int argc, char* argv[] )
{
	vector<string> exclusion_set;

	if( argc < 2 )
	{
		cout << "USAGE: freq.cpp <input file>\n";
		cout << "OR\n";
		cout << "USAGE: freq.cpp <input file> <exclusive list file>\n";
		exit(1);
	}
	else if( argc == 2 )
	{
		exclusion_set.push_back("a");
		exclusion_set.push_back("an");
		exclusion_set.push_back("or");
		exclusion_set.push_back("the");
		exclusion_set.push_back("and");
		exclusion_set.push_back("but");
		exclusion_set.push_back("");
		exclusion_set.push_back(" ");
	}
	else if( argc == 3 )
	{
		string word;
		char* excludedWords = argv[2];
		ifstream file(excludedWords);
		if( file.is_open() )
		{
			while( getline(file,word) )
			{
				exclusion_set.push_back(word);
			}
		}
		else
		{
			cout << "Unable to open exclusive set file\n";
			exit(1);
		}
	}

	string word;
	map<string,int> map;

	char* fileName = argv[1];
	ifstream file(fileName);
	if( file.is_open() )
	{
		while( getline(file,word) )
		{
			if( !isExcluded( word, exclusion_set ) )
			{
				map[word]++;
			}
		}

		file.close();
	}
	else
	{
		cout << "Unable to open input file\n";
		exit(1);
	}

	printMap( map );

	return 0;
}
