#/*
#* Course: CS 100 Fall 2013
#*
#* First Name: Justin
#* Last Name: Cano
#* Username: canoj
#* email address: jcano001@ucr.edu
#*
#*
#* Assignment: Homework #7
#*
#* I hereby certify that the contents of this file represent
#* my own original individual work. Nowhere herein is there
#* code from any outside resources such as another individual,
#* a website, or publishings unless specifically designated as
#* permissible by the instructor or TA.*/ 
#! /bin/bash
directories=$@
numSubdirectories=0
numFiles=0

function checkDir ( ) {
	local dir=$1
	for f in "$dir"/*
	do
		if [ -d $f ]
		then
			((numSubdirectories++))
			checkDir $f
		fi
	done
}

if [ $# -lt 1 ]
then
	echo "USAGE: bash dircheck.sh <dir1> <dir2> ..."
else
	for directory in ${directories[*]}
	do
		echo $directory
		checkDir $directory
		echo "Files with size 0:"
		for f in "$directory"/*
		do
			if [ -f $f ]
			then
				((numFiles++))
			fi
			if [ ! -s $f ]
			then
				echo $f
			fi
		done
		echo "Subdirectories: " $numSubdirectories
		echo "Files: " $numFiles
		numSubdirectories=0
		numFiles=0
		echo "Kilobytes used by "$directory": "
		du $directory
	done
fi
