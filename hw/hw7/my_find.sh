#/*
#* Course: CS 100 Fall 2013
#*
#* First Name: Justin
#* Last Name: Cano
#* Username: canoj
#* email address: jcano001@ucr.edu
#*
#*
#* Assignment: Homework #7
#*
#* I hereby certify that the contents of this file represent
#* my own original individual work. Nowhere herein is there
#* code from any outside resources such as another individual,
#* a website, or publishings unless specifically designated as
#* permissible by the instructor or TA.*/ 
#! /bin/bash
a=$@
args=($a)
directory=$1
name=""
type=""
print=""
c=""
command=""
target=""
for last; do true; done
targeted=$last

function checkDir ( ) {
	local dir=$1
	for f in "$dir"/*
	do
		temp_name=("$dir/$name")
		if [ -z $name ] # if -name is not set, print name of file
		then
			if [ -z $type ]
			then
				if [ -z $print ]
				then
					echo $f
					target=$f
				fi
			elif [ "$type" == "f" ]
			then
				if [ -f $f ]
				then
					echo $f
					target=$f
				fi
			elif [ "$type" == "d" ]
			then
				if [ -d $f ]
				then
					echo $f
					target=$f
				fi
			fi
		elif [ "$temp_name" == "$f" ] # else if -name set, print name of file if file is the same as name
		then
			if [ -z $type ]
			then
				echo $f
				target=$f
			elif [ "$type" == "f" ]
			then
				if [ -f $f ]
				then
					echo $f	
					target=$f
				fi
			elif [ "$type" == "d" ]
			then
				if [ -d $f ]
				then
					echo $f
					target=$f
				fi
			fi
		fi

		if [ -d $f ]
		then
			checkDir $f
		fi
		if [ "$targeted" == "{}" ]
		then
			$command $target
		fi
	done
}

if [ $# -lt 1 ]
then
	directory="."
fi
if [ ! -d $1 ]
then
	directory="."
fi

i=0
for arg in ${args[*]}
do
	case "$arg" in
		(-name) name=${args[(($i+1))]} ;;
		(-type) type=${args[(($i+1))]} ;;
		(-print) print="print" ;;
		(-exec) c="exec" ;;
		(*)
			if [ "$c" == "exec" ]
			then
				if [ "$arg" != "{}" ]
				then
					command=("$command $arg")
				fi
			fi
	esac
	((i=i+1))
done
echo $directory
checkDir $directory
