/* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #6
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/
#include "Matrix.h"
using namespace std;

template < class Element >
Matrix<Element>::Matrix( int newRows, int newCols )
 : rows( newRows ), cols( newCols ), m(newRows)
{
//	m = new Array< Element > (newRows);
//	for( int i = 0; i < newRows; i++ )
//		m[i] = new int[newCols];
}

template < class Element >
int Matrix<Element>::numRows()
{
	return rows;
}

template < class Element >
int Matrix<Element>::numCols()
{
	return cols;
}


template class Matrix< int >;
//template class Matrix< char >;
//template class Matrix< double >;

