/* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #6
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/
#include <iostream>
#include <cassert>
#include <iomanip>

using namespace std;

template < typename Type >
class Array
{
private:
	int len;
	Type* buf;
	bool inBounds( int i )
	{
		return 0 <= i && i < len;
	}

public:
	Array( int newLen = 0 )
 	 : len( newLen ), buf( new Type [newLen] ) {}
	Array( Array &l )
	 : len( l.len ), buf( new Type[l.len] )
	{
		for( int i = 0; i < l.len; i++ )
			buf[i] = l.buf[i];
	}
	~Array()
	{
		delete[] buf;
	}

	int length()
	{
		return len;
	}

	// IndexOutOfBoundsException
	class BaseException {};
	class IndexOutOfBoundsException : public BaseException {};
	Type & operator [] ( int i  )
	 throw( IndexOutOfBoundsException )
	{
		if( !inBounds( i ) )
			throw IndexOutOfBoundsException();
		return buf[i];
	}
	void print( ostream &out )
	{
		for( int i = 0; i < len; i++ )
			out << setw(5) << buf[i];
	}
	friend ostream & operator << ( ostream &out, Array<Type> &a )
	{
		a.print( out );
		return out;
	}
};

/*template < typename Type >
Array<Type>::Array( int newLen )
 : len( newLen ), buf( new Type [newLen] ) {}

template < typename Type >
Array<Type>::Array( Array &l )
 : len( l.len ), buf( new Type[l.len] )
{
	for( int i = 0; i < l.len; i++ )
		buf[i] = l.buf[i];
}

template < typename Type >
int Array<Type>::length()
{
	return len;
}*/

