/* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #6
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/
#include <iostream>
#include "Matrix.h"

using namespace std;

template < class T >
void fillMatrix( Matrix<T> & m )
{
	int i,j;
	for( i = 0; i < m.numRows(); i++ )
		m[i][0] = T();
	for( j = 0; j < m.numCols(); j++ )
		m[0][j] = T();
	for( i = 1; i < m.numRows(); i++ )
		for( j = 1; j < m.numCols(); j++ )
			m[i][j] = T(i * j);
}

int main()
{
	Matrix<int> m(10,5);
	fillMatrix(m);
	cout << m;

	cout << endl;

	Matrix<double> M(8,10);
	fillMatrix(M);
	cout << M;


	while(1)
	{
		cout << "Enter an index x for matrix m: ";
		int x;
		cin >> x;
		cout << "Enter an index y for matrix m: ";
		int y;
		cin >> y;
		try
		{
			cout<<"m["<<x<<"]["<<y<<"]: ";
			cout << m[x][y] << endl;
		}
		catch( Matrix<int>::IndexOutOfBoundsException & e )
		{
			cout << "ERROR: index out of bounds\n";
		}	
		cout << "Enter an index x for matrix M: ";
		cin >> x;
		cout << "Enter an index y for matrix M: ";
		cin >> y;
		try
		{
			cout<<"M["<<x<<"]["<<y<<"]: ";
			cout << M[x][y] << endl;
		}
		catch( Matrix<int>::IndexOutOfBoundsException & e )
		{
			cout << "ERROR: index out of bounds\n";
		}	
	}
	return 0;
}
