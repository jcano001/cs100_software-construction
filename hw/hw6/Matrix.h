/* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #6
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/
#include <iostream>
#include <cassert>
#include <iomanip>
#include "Array.h"

using namespace std;

template class Array<int>;
template class Array<char>;
template class Array<double>;

template < class Element >
class Matrix
{
private:
	int rows;
	int cols;
	//define m as an Array of Array pointers using Array.h
	Array< Array<Element>* > m;
	bool inBounds( int i )
	{
		return 0 <= i && i < rows;
	}
public:
	Matrix( int newRows, int newCols )
	 : rows( newRows ), cols( newCols ), m(newRows)
	{
		for( int i = 0; i < newRows; i++ )
			m[i] = new Array< Element > (newCols);
	}
	int numRows()
	{
		return rows;
	}
	int numCols()
	{
		return cols;
	}

	// IndexOutOfBoundsException
	class BaseException {};
	class IndexOutOfBoundsException : public BaseException {};
	Array< Element > & operator [] ( int row )
	 throw( IndexOutOfBoundsException )
	{
		if( !inBounds( row ) )
			throw IndexOutOfBoundsException();
		return *m[row];
	}

	friend ostream & operator << ( ostream & out, Matrix & m )
	{
		for( int i = 0; i < m.rows; i++ )
			cout << m[i] << endl;
		return out;
	}
};
