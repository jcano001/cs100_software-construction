/* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #6
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA.*/
#include "Array.h"
using namespace std;

template < typename Type >
Array<Type>::Array( int newLen )
 : len( newLen ), buf( new Type [newLen] ) {}

template < typename Type >
Array<Type>::Array( Array &l )
 : len( l.len ), buf( new Type[l.len] )
{
	for( int i = 0; i < l.len; i++ )
		buf[i] = l.buf[i];
}

template class Array<int>;
template class Array<char>;
template class Array<double>;

/*
template class Array<int**>;
template class Array<char**>;
template class Array<double**>;

template class Array<int*>;
template class Array<char*>;
template class Array<double*>;
*/
/*
template < typename Type >
Array<Type>::~Array()
{
	delete[] buf;
}

template class ~Array<int>;
*/



template < typename Type >
int Array<Type>::length()
{
	return len;
}

