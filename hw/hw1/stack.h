#define STACK_CAPACITY 1000
class Stack
{
public:
	Stack() // constructor for a stack
	{
		stack = new char[STACK_CAPACITY]; 
		numElements = 0;
		topIndex = -1;
	}
	void push( char c ) // adds c to the top of the stack
	{
		if( numElements < STACK_CAPACITY ) // first, check to see if stack is not full
		{
			numElements++;
			stack[++topIndex]=c;
		}
	}
	char pop() // removes top element, returns it
	{
		if( !isEmpty() ) // first, check to see if stack is not empty
		{
			numElements--;
			return stack[topIndex--];
		}
		else{
			topIndex = 0;
			return -1;
		}
	}
	char top() // returns top element
	{
		return stack[topIndex];
	}
	bool isEmpty() // returns true iff the stack is empty
	{
		return (numElements==0);
	}
	~Stack() // desctructor for a stack
	{
		delete [] stack;
	}

private:
	char* stack;
	int numElements;
	int topIndex;
};
