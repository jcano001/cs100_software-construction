/*
  Name: Justin Cano
  Lab Section: 22
  Assignment: Homework 1 Rolodex

  I acknowledge all content contained herein is my own original work
 */
#include <iostream>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

class contactNode
{
public:
	contactNode()
	 :fname(),lname(),address(),phone(),next(NULL)
	{
	}

	contactNode(string f, string l, string addy, string ph)
		:fname(f), lname(l), address(addy), phone(ph), next(NULL)
	{
	}

	~contactNode()
	{
	}

	contactNode* getNext()
	{
		return next;
	}

	void setNext(contactNode* nextNode)
	{
		next = nextNode;
	}

	string getFirstName()
	{
		return fname;
	}

	string getLastName()
	{
		return lname;
	}

	string getAddress()
	{
		return address;
	}

	string getPhone()
	{
		return phone;
	}

	void print()
	{
		cout << "***** " << fname << " " << lname << " *****" << endl;
		cout << " Address: " << address << endl << " Phone #: " << phone << endl;
		cout << "------------------------------" << endl;
	}

private:
	string fname;
	string lname;
	string address;
	string phone;

	contactNode* next;
};

class Rolodex
{
public:
	Rolodex()
	 :head(NULL),size(0),modified(false)
	{
	}

	void insert(string fname, string lname, string address, string phone)
	{
		contactNode * newContact = new contactNode(fname,lname,address,phone);
		// case 1: empty list
		if(!head)
		{
			head = newContact;
			return;
		}

		contactNode* previous = head;
		// for loop to interate through for other cases
		for( contactNode* i = head; i; i = i->getNext() )
		{
			if( lname <= i->getLastName() )
			{
				// case 2: insert to top of list
				if( i==head )
				{
					newContact->setNext(i);
					head = newContact;
					break;
				}
				// case 3: insert in between nodes
				else
				{
					newContact->setNext(previous->getNext());
					previous->setNext(newContact);
					break;
				}
			}

			// case 3: list has at least 1 item,
			//		   append new contact to end of list
			if( i->getNext() == NULL && lname > i->getLastName() )
			{
				i->setNext(newContact);
				i = newContact;
				break;
			}

			if( lname > i->getLastName() )
			{
				previous = i;
			}
		}
		size++;
		modify(true);
	}

	contactNode* search(string query)
	{
		for( contactNode* i = head; i; i = i->getNext() )
			if( i->getLastName() == query ) return i;

		cout << "404: Contact not found\n";
		return NULL;
	}

	bool removeContact(string query)
       {
	        contactNode* temp = search(query);
		contactNode* previous = head;
		if(temp)
		{
			for( contactNode* i = head; i; i = i->getNext() )
			{
				if( i== temp )
				{
					previous->setNext(temp->getNext());
					if( temp == head ) head = head->getNext();
					delete temp;
				}
				else
				{
					previous = i;
				}
			}
			modified = true;
			return true;
		}
		return false;
	}

	~Rolodex()
	{
		contactNode* i = head;
		contactNode* temp = NULL;
		while(i)
		{
			temp = i;
			i = i->getNext();
			delete temp;
		}
	}

	void print()
	{
		for(contactNode* i = head; i; i = i->getNext())
			i->print();
	}

	void save(string fileName)
	{
		ofstream file;
		file.open(fileName.c_str());
		for(contactNode* i = head; i; i = i->getNext())
		{
			file << i->getFirstName() << endl << i->getLastName()
				<< endl << i->getAddress() << endl << i->getPhone() << endl;
		}
		file.close();
		modify(false);
	}

	void load(string fileName)
	{
		ifstream file(fileName.c_str());
		if(file)
		{
			string f, l, a, p;
			cout << endl << "'" << fileName << "' loaded." << endl;
			empty();
			while( file.good() )
			{
				getline(file,f);
				getline(file,l);
				getline(file,a);
				getline(file,p);
				if( f != "" ) insert(f,l,a,p);
			}
		}
		else cout << endl << "'" << fileName << "' unable to load." <<  endl;

		file.close();
		modify(false);
	}

	bool isModified()
	{
		return modified;
	}

	void modify(bool m)
	{
		modified = m;
	}

private:
	contactNode* head;
	int size;
	bool modified;

	void empty() // empties rolodex in order to load a new one
	{
		contactNode* temp = NULL;
		contactNode* i = head;
		while(i)
		{
			temp = i;
			i = i->getNext();
			delete temp;
		}
		head = NULL;
	}
};


int main()
{
	Rolodex r;
	char option='-1';

	string fname;
	string lname;
	string address;
	string phone;
	string fileName;


	cout << endl<< endl
	     << "********************" << endl
	     << "*                  *" << endl
	     << "*                  *" << endl
	     << "*     Rolodex      *" << endl
	     << "*                  *" << endl
	     << "*                  *" << endl
	     << "********************" << endl << endl << endl;


	


	while( option != 'q' )
	{
		switch(option)
		{
			case 'f':
				cout << "Enter the last name of whose address you want to look up (case sensitive):\n$ ";
				cin >> lname;
				cout << endl;
				r.search(lname)->print();
				cout << endl;
				cout << "$ ";
				while(!(cin>>option));
				break;
			case 'd':
				cout << "Enter the last name of whose address you want to delete (case sensitive):\n$ ";
				cin >> lname;
				cout << endl;
				fname = r.search(lname)->getFirstName();
				if(r.removeContact(lname)) cout << fname << " " << lname << " deleted.\n";
				cout << "$ ";
				while(!(cin>>option));

				break;
			case 'i':
				cout << "Insert new address\n First Name: ";
				cin >> fname;
				cout << " Last Name: ";				
				cin >> lname;
				cout << " Address: ";
				cin.ignore();
				getline(cin,address);
				cout << " Phone: ";				
				cin >> phone;

				r.insert(fname,lname,address,phone);

				cout << endl;
				cout << "$ ";
				while(!(cin>>option));
				break;
			case 'p':
				cout << endl;
				r.print();
				cout << endl;
				cout << endl;
				cout << "$ ";
				while(!(cin>>option));
				break;
			case 'l':
				cout << "Specify the file you want to load (do not include extension, i.e. .rdex):\n$ ";
				cin >> fileName; 
				fileName += ".rdex";
				r.load(fileName);

				cout << endl;
				cout << "$ ";
				while(!(cin>>option));
				break;
			case 's':
				cout << "What would you like to save the file as (do not include extension, i.e. .rdex)?\n$ ";
				cin >> fileName;
				fileName += ".rdex";
				r.save(fileName);

				cout << endl;
				cout << "$ ";
				while(!(cin>>option));
				break;
		case 'q':
		  break;
		case '-1':
		  cout << "***** MENU *****" << endl
			<< "Select an option:" << endl
			<< "i - insert a new address" << endl
			<< "f - find a given address" << endl
			<< "d - delete a given address" << endl
			<< "p - print all cards in the rolodex" << endl
			<< "l - load addresses from a given rolodex file" << endl
			<< "s - save the addresses to a specifed rolodex file" << endl
		     << "    (will be saved as a .rdex file)" << endl
		       << "q - quit the program" << endl << endl << "$ ";
		  while(!(cin>>option));
		  break;
		  
		default:
		  cout << "    //!*!*!*!*!*!*!*!*!*!*!*!*!*!//" << endl
		       << "   //                           //" << endl
		       << "  //   Select another option   //" << endl
		       << " //                           //" << endl
		       << "//*!*!*!*!*!*!*!*!*!*!*!*!*!*//" << endl;
		  option = '-1';
		  break;

				cout << endl;
				break;
		}
	}
	if( r.isModified() )
	{
		do
		{
			cout << "Rolodex not saved! Would you like to save? (y/n)\n$ ";
			cin >> option;
			switch(option)
			{
				case 'y':
					cout << "What would you like to save the file as (do not include extension, i.e. .rdex)?\n$ ";
					cin >> fileName;
					fileName += ".rdex";
					r.save(fileName);

					cout << "Rolodex saved as " << fileName << endl;
					break;
				case 'n':
					cout << "Exiting\n";
					r.modify(false);
					break;
				default:
					cout << "Invalid option. Would you like to save? (y/n)\n$ ";
			}
		} while( r.isModified());
	}
	return 0;
}
