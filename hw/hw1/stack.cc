#include "stack.h"
#include <iostream>

using namespace std;

int main()
{

	cout << "Input character sequence (exit on CTRL + D)\n";

	Stack s;
	
	char input;

	while( cin >> input )
	{
		s.push( input );
	}
	cout << endl;
	while( !s.isEmpty() )
	{
		cout << s.pop();
	}

	cout << endl;

	return 0;
}