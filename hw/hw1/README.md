# STACK
stack.h is my own C++ implementation of a character stack, which includes the following basic methods:
======
* `push(char c)`
> pushes c to top of stack
* `pop()`
> returns and removes the element at the top of the stack
* `top()`
> returns element at top of the stack (does not remove)
* `isEmpty()`
> returns if the stack is empty or not

# ROLODEX
rolodex.cc is a C++ rolodex application. 
=====
The application displays a user interactive menu and command prompt. Valid commands are:
=====
* `i`
> insert a new rolodex entry
* `f`
> find a rolodex entry by last name
* `d`
> delete a rolodex entry by last name
* `p`
> print out all rolodex entries
* `l`
> load a saved rolodex file
* `s`
> save rolodex (will be saved as a .rdex file)
* `q`
> quit the program. If any modifcations have been made to the current rolodex, program will prompt user to save

# INSTALL
These programs were created on a Linux machine and compiles with g++
=====
``g++ stack.cc``
=====
``g++ rolodex.cc``