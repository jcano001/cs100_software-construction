# Coins
Coins is an object that acts like a coin purse. It can hold Quarters, Dimes, Nickels and Pennies.
=====
`main.cc`, `betterMain.cc`, and `bestMain.cc` are all test harnesses that test the functionality of the Coins object.
=====
* `main.cc`
> This is a basic test harness for Coins. It instantiates a Coins object called "pocket" with 100 quarters, 10 dimes, 10 nickels, and 100 pennies. The program continues to extract enough change from your pocket to pay for a candy bar that costs 482 cents (what a very expensive candy bar o.O)
* `betterMain.cc`
> This is a better test harness for Coins. It instantiates Coins objects "pocket" and "piggyBank". The program continues to extract enough change from your pocket to pay for a bag of chips that costs 68 cents. After, the program withdraws 142 cents and puts the change into your pocket. Finally, the program assumes that you found 3 dimes, 1 nickel, and 7 pennies while vacuuming your sofa and safely deposits them into your piggyBank.
* `bestMain.cc`
> This is the best test harness for Coins. `bestMain.cc` is a user friendly program that acts as a Coin Bank. It displays a menu and prompts the user for input. The program first initialzes an empty coins object. The user can then view the balance and deposit/withdraw coins. Note that when withdrawing, the user is not allowed withdraw more than the available balance, the amount withdrawn uses the optimal amount of coins, and can only withdraw using what coins are available. For example, if the user withdraws 167 cents and all coins are available except for pennies, the amount withdrawn is 165 cents using 6 quarters, 1 dime and 1 nickel.

#MY_LS
`my_ls` is a subset of the UNIX command `ls`.
=====
Usage:
`my_ls <directory path>`

#INSTALL
The included `Makefile` compiles the programs on Linux machines using g++
=====
`make all` or `make`
