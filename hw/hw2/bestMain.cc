/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #2
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
#include "Coins.h"

const int CENTS_FOR_CHIPS = 68;

int main()
{ 
	cout << "********************" << endl
		<< "*                  *" << endl
		<< "*    Coin Bank     *" << endl
		<< "*                  *" << endl
		<< "********************" << endl << endl;
		

	Coins myCoins;
	char option = '-1';

	int quarters, dimes, nickels, pennies = 0;

	while( option != 'q' )
	{
		switch( option )
		{
			case '-1':
				cout << "***** MENU *****" << endl
					<< "Select an option:" << endl
					<< "b - view balance" << endl
					<< "d - make a deposit" << endl
					<< "w - withdraw (extract) change" << endl
					<< "q - quit" << endl << endl << "$ ";
				while( !(cin >> option) );
				break;
			case 'b':
				cout << "Your coin balance:\n" << myCoins << endl << "Total Balance: " << myCoins.balance() << endl << endl << "$ ";
				while( !(cin >> option) );
				break;
			case 'd':
				int q,d,n,p;
				cout << "Please enter the amount of coins you would like to deposit:\nQuarters: ";
				cin >> q;
				cout << "Dimes: ";
				cin >> d;
				cout << "Nickels: ";
				cin >> n;
				cout << "Pennies: ";
				cin >> p;
				myCoins.depositChange( Coins(q,d,n,p)  );
				cout << endl << "$ ";
				while( !(cin >> option) );
				break;
			case 'w':
				int amount;
				cout << "How much would you like to withdraw (in cents)? ";
				cin >> amount;
				if( myCoins.hasSufficientAmount( amount ) )
				{
					Coins withdrawn = myCoins.extractChange( amount );
					cout << "You withdrew " << withdrawn << endl << endl << "$ ";
				}
				else cout << "Insufficient funds.\n\n$ ";
				while( !(cin >> option) );
				break;
			default:
				cout << "Invalid option.\n\n$ ";
				option = '-1';
				break;
		}
	}
}
