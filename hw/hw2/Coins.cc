/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #2
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
#include "Coins.h"

Coins::Coins( int q, int d, int n, int p )
 :quarters(q), dimes(d), nickels(n), pennies(p) {}

Coins::Coins()
 :quarters(0), dimes(0), nickels(0), pennies(0) {}

void Coins::depositChange( Coins c )
{
	this->quarters += c.getQuarters();
	this->dimes += c.getDimes();
	this->nickels += c.getNickels();
	this->pennies += c.getPennies();
}

bool Coins::hasSufficientAmount( int amount )
{
	int sum = (quarters*CENTS_PER_QUARTER)
		+ (dimes*CENTS_PER_DIME)
		+ (nickels*CENTS_PER_NICKEL)
		+ (pennies);

	return (sum >= amount);
}

int Coins::balance()
{
	return (quarters*CENTS_PER_QUARTER)
		+ (dimes*CENTS_PER_DIME)
		+ (nickels*CENTS_PER_NICKEL)
		+ (pennies);
}

Coins Coins::extractChange( int amount )
{
	int numCoins = 0;

	int extractedQuarters, extractedDimes, extractedNickels, extractedPennies = 0;

	if( hasSufficientAmount( amount ) )
	{
		numCoins = amount / CENTS_PER_QUARTER;
		if( numCoins >= quarters )
		{
			amount -= quarters*CENTS_PER_QUARTER;
			extractedQuarters = quarters;
			quarters = 0;
		}
		else
		{
			amount -= numCoins*CENTS_PER_QUARTER;
			extractedQuarters = numCoins;
			quarters -= numCoins;
		}

		numCoins = amount / CENTS_PER_DIME;
		if( numCoins >= dimes )
		{
			amount -= dimes*CENTS_PER_DIME;
			extractedDimes = dimes;
			dimes = 0;
		}
		else
		{
			amount -= numCoins*CENTS_PER_DIME;
			extractedDimes = numCoins;
			dimes -= numCoins;
		}

		numCoins = amount / CENTS_PER_NICKEL;
		if( numCoins >= nickels )
		{
			amount -= nickels*CENTS_PER_NICKEL;
			extractedNickels = nickels;
			nickels = 0;
		}
		else
		{
			amount -= numCoins*CENTS_PER_NICKEL;
			extractedNickels = numCoins;
			nickels -= numCoins;
		}

		numCoins = amount;
		if( numCoins >= pennies )
		{
			amount -= pennies;
			extractedPennies = pennies;
			pennies = 0;
		}
		else
		{
			amount -= numCoins;
			extractedPennies = numCoins;
			pennies -= numCoins;
		}

		return Coins(extractedQuarters, extractedDimes, extractedNickels, extractedPennies);
	}
	
	cout << "Insufficient change\n";
	return Coins();
}

int Coins::getQuarters()
{
	return quarters;
}

int Coins::getDimes()
{
	return dimes;
}

int Coins::getNickels()
{
	return nickels;
}

int Coins::getPennies()
{
	return pennies;
}

void Coins::print( ostream & out )
{
	out << quarters << " q, " << dimes << " d, " << nickels << " n, " << pennies << " p";
}

ostream & operator << ( ostream & out, Coins & c )
{
	c.print( out );
	return out;
}
