/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #2
*
* This exercise is my own implementation of a subset of the
* Unix command ls with flags -lr and -R
*
* alias my_ls <dir> = "ls -rR <dir>"
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>

using namespace std;

const char* INDENT = "   ";
int gIndentCounter = 0;

char* getMonth(int i)
{
	char* months[12] = {"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
	return months[i];
}

void readContents( char* path )
{
	struct stat buf;

	if( stat(path,&buf) != -1 ) {
		// if path is a directory, open it and reference to a file inside it
		if(S_ISDIR(buf.st_mode)) {
			DIR* temp_dirp = opendir(path);
			dirent* temp_direntp;
			char fullPath[128];

			cout << path << endl;

			gIndentCounter++;
	
			while( (temp_direntp = readdir(temp_dirp)) ) {
				if( strcmp(temp_direntp->d_name, ".") && strcmp(temp_direntp->d_name, "..") ) {
					strcpy(fullPath,path);
					strcat(fullPath,"/");
					strcat(fullPath,temp_direntp->d_name);
					readContents(fullPath);
				}
			}
			gIndentCounter--;
			closedir(temp_dirp);
		}
		// else, list file details
		else {
			tm *ltm = localtime(&buf.st_mtime);
			for(int i = 0; i < gIndentCounter; i++)
				cout << INDENT;
			//cout << "Stat for " << path << ": Links " << buf.st_nlink << ", Size " << buf.st_size << endl;
			cout << buf.st_mode << " " << buf.st_nlink << " " << buf.st_size << " " << getMonth(ltm->tm_mon) << " " 
				<< ltm->tm_mday << " " << ltm->tm_hour << ":" << ltm->tm_min << ":" << ltm->tm_sec << " " << path << endl;
		}
	}
	else
		cerr << "Error: " << errno << " opening " << path << endl;
}

int main( int argc, char* argv[] )
{
	char* path;
	DIR* dirp;
	char fullPath[128];

	// if directory is specified
	if( argc > 1 ) 
		path = argv[1];
	else
		path = ".";
		
	// if directory is not valid, return errno
	if( !(dirp = opendir(path)) ) {
		cerr << "Error(" << errno << ") opening " << path << endl;
		return errno;
	}

	// valid directory
	dirent* direntp;

	cout << path << endl;

	while( (direntp = readdir(dirp)) ) {
		if( strcmp(direntp->d_name, ".") && strcmp(direntp->d_name, "..") )
		{
			strcpy(fullPath,path);
			strcat(fullPath,"/");
			strcat(fullPath,direntp->d_name);
			readContents(fullPath);
		}
	}

	closedir(dirp);
	return 0;
}
