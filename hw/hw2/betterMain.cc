/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #2
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
#include "Coins.h"

const int CENTS_FOR_CHIPS = 68;

int main()
{ 
	/// Creates a Coins object called 'pocket' and 'piggyBank.'
	Coins pocket( 5, 3, 6, 8 );
	Coins piggyBank( 50, 50, 50, 50 );
	cout << "I started with " << pocket << " in my pocket and " << piggyBank << " in my piggy bank." << endl;

	cout << endl;
	// Creates a Coins object called payForChips and initializes it.
	Coins payForChips = pocket.extractChange(CENTS_FOR_CHIPS);
	cout << "I bought a bag of chips for " << CENTS_FOR_CHIPS << " cents using " << payForChips << endl;
	cout << "I have " << pocket << " left in my pocket" << endl;

	cout << endl;
	int withdrawCoins = 142;
	pocket.depositChange( piggyBank.extractChange( withdrawCoins ) );
	cout << "I withdrew " << withdrawCoins << " cents from my piggyBank to my pocket." << endl;
	cout << "I now have " << pocket << " in my pocket and " << piggyBank << " left in my piggy bank." << endl;
	
	cout << endl;
	Coins changeFound(0,3,1,7);
	piggyBank.depositChange( changeFound );
	cout << "I found " << changeFound << " coins while vacuuming my sofa!\nI deposited it and now have " << piggyBank << " in my piggy bank." << endl;
	return 0;
}
