/*
* Course: CS 100 Fall 2013
*
* First Name: Justin
* Last Name: Cano
* Username: canoj
* email address: jcano001@ucr.edu
*
*
* Assignment: Homework #2
*
* I hereby certify that the contents of this file represent
* my own original individual work. Nowhere herein is there
* code from any outside resources such as another individual,
* a website, or publishings unless specifically designated as
* permissible by the instructor or TA. */
#include <iostream>

using namespace std;

const int CENTS_PER_QUARTER = 25, CENTS_PER_DIME = 10, CENTS_PER_NICKEL = 5;

class Coins
{
	public:
		Coins( int q, int d, int n, int p );
		Coins();
		void depositChange( Coins c );
		bool hasSufficientAmount( int amount );
		int balance();
		Coins extractChange( int amount );
		int getQuarters();
		int getDimes();
		int getNickels();
		int getPennies();
		void print( ostream & out );
	private:
		int quarters, dimes, nickels, pennies;
};

ostream & operator << ( ostream & out, Coins & c );
