#include <iostream>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

int main() {
	int pid;
	char letters[4] = { 'A', 'B', 'C', 'D' };
	for( int i = 0; i < 4; i++ ) {
		pid = fork();
		switch(pid) {
			case -1:
				cout << "ERROR: fork failed" << endl;
				return -1;
			case 0:
				for( int j = 0; j < 10000; j++ ) {
					cout << letters[i] << flush;
				}
				exit(0);
			default:
				cout << pid << endl;
				break;
		}
	}
	int status;
	while( wait(&status) != -1 ); //the parent process should wait for all of its children to terminate
	return 0;
}
