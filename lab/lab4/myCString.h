#include <iostream>

using namespace std;

unsigned my_strlen(char * str)
{
	char * temp = str;
	unsigned count = 0;
	while(*(temp+count) != '\0')
	{
		++ count;
	}
	return count;
}


char * my_strcat(char * destination, char * source)
{
	int size = my_strlen(destination)+my_strlen(source);
	char buf[size];
	int i = 0;
	while( i < my_strlen(destination) )
	{
		*(buf+i) = *(destination+i);
		i++;
	}
	
	int count = 0;
	while( count < my_strlen(source) )
	{
		 *(buf+i) = *(source+count);
		i++;
		count++;
	}

	buf[size] = '\0';

	destination = buf;
	cout << buf;
	return destination;
}

int my_strcmp(char * str1, char * str2)
{

	for( int i = 0; *(str1+i) != '\0' && *(str2+i) != '\0'; i++ )
	{
		if( *(str1+i) != *(str2+i) )
		{
			if( *(str1+i) > *(str2+i) )
				return 1;
			else 
				return -1;
		}
	}

	if( my_strlen(str1) != my_strlen(str2) )
	{
		if( my_strlen(str1) > my_strlen(str2) )
			return 1;
		else if( my_strlen(str1) < my_strlen(str2) )
			return -1;
	}
	return 0;
}

char * my_strcpy(char * destination, char * source)
{
	int size = my_strlen(source);
	char buf[size];
	
	for( int i = 0; *(source+i) != '\0'; i++ )
	{
		buf[i] = source[i];
	}
	buf[size] = '\0';
	destination = buf;
	cout << buf << endl;
	return destination;
}

char * my_strchr( char * str, int character )
{
	char * p = str;
	for( int i = 0; *(p+i) != '\0'; i++ )
	{
		if( *(p+i) == character )
			return p+i;
	}
	return NULL;
}

char * my_strstr( char * str1, char * str2 )
{
	int tracker = 0;
	for( int i = 0; i < my_strlen(str1); i++ )
	{
		if( *(str1+i) == *(str2) )
		{
			int j = 0;
			while( *(str1+i+j) == *(str2+j) )
			{
				tracker++;
				j++;
			}
			if( tracker == my_strlen(str2) )
			{
				return str1+i;
			}
			else
			{
				tracker = 0;
				j = 0;
			}
		}
	}
	return NULL;
}
