#include "myCString.h"

int main() {

	char * s = "abc";
	cout << "s = \"Hello\"" << endl;
	char* s2 = "ab";
	cout << "s2 = \"World!\"" << endl;

	cout << "strlen(s):\n";
	cout <<  my_strlen(s) << " " << strlen(s) << endl;

	cout << "strcmp(s,s2):\n";
	cout << my_strcmp(s,s2) << " " << strcmp(s,s2) << endl;
	cout << "strcmp(s2,s):\n";
	cout << my_strcmp(s2,s) << " " << strcmp(s2,s) << endl;
	cout << "strcmp(s,s):\n";
	cout << my_strcmp(s,s) << " " << strcmp(s,s) << endl;

	cout << "strchr(s,'e')\n";
	cout << "char * p = my_strchr(s,'e')\n";
	char * p = my_strchr(s,'e');
	cout << p << endl;
	cout << "char * q = strchr(s,'e')\n";
	char * q = strchr(s,'e');
	cout << q << endl;

	char * str1 = "123Hey345Hello67";
	cout << "str1 = \"123Hey345Hello67\"" << endl;
	cout << "char * p1 = my_strstr(str1,s)\n";
	char * p1 = my_strstr(str1,s);
	cout << p1 << endl;
	cout << "char * q1 = strstr(str1,s)\n";
	char * q1 = strstr(str1,s);
	cout << q1 << endl;

	cout << "strcat(s,s2)\n";
	my_strcat(s,s2);
//	strcat(s,s2);
	cout << s << endl;

	char * str2 = "a";
	char * str3 = "Hi";
	cout << "char * str2 = \"a\"" << endl;
	cout << "char * str3 = \"Hi\"" << endl;
	cout << "strcpy(str2,str3)\n";
	my_strcpy(str2,str3);
	cout << endl;
	
	return 0;
}
