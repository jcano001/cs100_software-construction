// Justin Cano 860945660
// Nitin Kesarwani 860987306
// John Ericta 860803073
#include <iostream>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <strings.h>

using namespace std;

#define MY_SOCK_PATH "my_path"
#define BACK_LOG 50
#define MAXDATASIZE 255

int main( int argc, char *argv[] )
{
	int pid;
//	char sock_path[] = "/my_path/";


	/* Server Side Variables */
	int sockfd;

	struct sockaddr_un serv_addr, cli_addr;
	socklen_t cli_addr_size;


	/* Client Side Variables */
	int clientfd;
	int numbytes;
	int client_buf[MAXDATASIZE];

	int size = 0;
	pid = fork();
	switch(pid) {
		case -1:
			cout << "ERROR: fork failed" << endl;
			return -1;
		case 0: /* Child - client */
			sleep(1);
			bzero((int*)&cli_addr,sizeof(struct sockaddr_un));
			clientfd = socket( AF_UNIX, SOCK_STREAM, 0 );
			cli_addr.sun_family = AF_UNIX;
			strncpy(cli_addr.sun_path,MY_SOCK_PATH,sizeof(cli_addr.sun_path)-1);
			size = sizeof(cli_addr);
			if( connect(clientfd,(struct sockaddr*) &cli_addr,size) == -1 )
				cout << "Connect Error\n";
			for( int i = 0; i < 5; i++ )
			{
				numbytes = read(clientfd,client_buf,MAXDATASIZE-1);
				if( numbytes == -1 )
				{
					cout << "Read Error\n";
				//	exit(1);
				}
				else
				{
					cout << "Read OK...\n";
					cout << (*client_buf) << endl;
				}
				sleep(1);
			}
			close(clientfd);
			exit(0);
		default: /* Parent - server */
			bzero((int*)&serv_addr,sizeof(struct sockaddr_un));
			sockfd = socket( AF_UNIX, SOCK_STREAM, 0 );
			serv_addr.sun_family = AF_UNIX;
			strncpy(serv_addr.sun_path,MY_SOCK_PATH,sizeof(serv_addr.sun_path)-1);
			size = sizeof(serv_addr);
			if( bind(sockfd, (struct sockaddr*) &serv_addr, size) == -1 )
				cout << "Bind Error\n";

			if( listen(sockfd, BACK_LOG) == -1 )
				cout << "Listen Error\n";

			cli_addr_size = sizeof(struct sockaddr_un);
			clientfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cli_addr_size);
			if( clientfd == -1 )
				cout << "Accept Error\n";
			int buf[5] = {1,2,3,4,5};
			for( int i = 0; i < 5; i++ )
			{
				if( write(clientfd,&buf[i],sizeof(int)) == -1 )
					cout << "Write Error\n";
				else {
					cout << "Write " << buf[i] << " is OK...\n";
				}
				sleep(1);
			}
			int status;
			while( wait(&status) != -1 ); //the parent process should wait for all of its children to terminate
			close(sockfd);
			break;
		}
	


	unlink(MY_SOCK_PATH);
	return 0;
}
