// Justin Cano 86094560
// Brady Leong 861020665
#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>

using namespace std;

class my_set
{
public:
	my_set() {}
	my_set( vector<int> v )
	{
		container.swap(v);
	}

	void my_union( my_set m1, my_set m2 )
	{
		vector<int> v1(m1.container);
		vector<int> v2(m2.container);
		sort(v1.begin(),v1.end());
		sort(v2.begin(),v2.end());

		vector<int>::iterator it;
		container.resize(v1.size()+v2.size());
		it = set_union(v1.begin(),v1.end(),v2.begin(),v2.end(),container.begin());

		container.resize(it-container.begin());
	}

	void my_intersection( my_set m1, my_set m2 )
	{
		vector<int> v1(m1.container);
		vector<int> v2(m2.container);
		sort(v1.begin(),v1.end());
		sort(v2.begin(),v2.end());

		vector<int>::iterator it;
		container.resize(v1.size()+v2.size());
		it = set_intersection(v1.begin(),v1.end(),v2.begin(),v2.end(),container.begin());

		container.resize(it-container.begin());
	}
	
	void my_difference( my_set m1, my_set m2 )
	{
		vector<int> v1(m1.container);
		vector<int> v2(m2.container);
		sort(v1.begin(),v1.end());
		sort(v2.begin(),v2.end());

		vector<int>::iterator it;
		container.resize(v1.size()+v2.size());
		it = set_difference(v1.begin(),v1.end(),v2.begin(),v2.end(),container.begin());

		container.resize(it-container.begin());
	}
	
	bool my_inclusion( my_set m1, my_set m2 ) // returns true of m1 is a subset of m2
	{
		vector<int> v1(m1.container);
		vector<int> v2(m2.container);
		sort(v1.begin(),v1.end());
		sort(v2.begin(),v2.end());

		vector<int>::iterator it;
		vector<int> result;
		result.resize(v1.size()+v2.size());
		it = set_intersection(v1.begin(),v1.end(),v2.begin(),v2.end(),result.begin());
		result.resize(it-result.begin());

		if( result.size() != v1.size() )
			return false;

		for( int i = 0; i < result.size(); i++ )
			if( result[i] != v1[i] )
				return false;

		return true;
	}

	void print() {
		for( vector<int>::iterator i = container.begin(); i < container.end(); i++ )
			cout << *i << ", ";
		cout << endl;
	}
private:
	vector<int> container;
};

void print( vector<int> v ) {
	for( vector<int>::iterator i = v.begin(); i < v.end(); i++ )
		cout << *i << ", ";
	cout << endl;
}


int main() {
	int first[] = {10,20,30};
	int second[] = {50,40,30,20,10};
	vector<int> v1;
	v1.push_back(10);
	v1.push_back(20);
	v1.push_back(30);

	vector<int> v2;
	v2.push_back(10);
	v2.push_back(20);
	v2.push_back(30);
	v2.push_back(40);
	v2.push_back(50);

	my_set m1(v1);
	my_set m2(v2);
	cout << "m1:\n";
	m1.print();
	cout << "m2:\n";
	m2.print();

	cout << endl;

	my_set m;
	cout << "m1 union m2:\n";
	m.my_union(m1,m2);
	m.print();

	my_set intersection;
	cout << "m1 intersection m2:\n";
	intersection.my_intersection(m1,m2);
	intersection.print();

	my_set difference;
	cout << "m1 difference m2:\n";
	difference.my_difference(m1,m2);
	difference.print();

	my_set inclusion;
	cout << "is m2 inclusive of m1?\n";	
	bool b = inclusion.my_inclusion(m1,m2);
	if( b )
		cout << "inclusion\n";
	else
		cout << "not inclusion\n";


	return 0;
}
