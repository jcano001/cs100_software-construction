// Justin Cano 86094560
// Brady Leong 861020665
#include <iostream>
#include <vector>
#include <deque>
#include <algorithm>

using namespace std;

bool highPriority(int i, int j) { return (i>j); }

class PriorityQueue
{
public:
	PriorityQueue() {}
	PriorityQueue(vector<int> v)
	{
		for(int i = 0; i < v.size(); i++)
			priorityQueue.push_back(v[i]);
	}

	void prioritize()
	{
		sort(priorityQueue.begin(),priorityQueue.end(),highPriority);
	}

	void print()
	{
		for(deque<int>::iterator it = priorityQueue.begin(); it != priorityQueue.end(); it++)
		{
			cout << *it << ", ";
		}
		cout << endl;
	}
private:
	deque<int> priorityQueue;
};

void print( vector<char> v ) {
	for( vector<char>::iterator i = v.begin(); i < v.end(); i++ )
		cout << *i << ", ";
	cout << endl;
}

int main() {
	vector<int> myints;
	myints.push_back(16);
	myints.push_back(2);
	myints.push_back(77);
	myints.push_back(29);
	myints.push_back(33);
	myints.push_back(67);
	myints.push_back(11);

	PriorityQueue p(myints);
//	deque<int> priorityQueue(myints,myints+sizeof(myints)/sizeof(int));
	p.print();
	p.prioritize();
	cout << endl;
	p.print();
	

	return 0;
}
