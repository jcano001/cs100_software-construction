// Justin Cano 86094560
// Brady Leong 861020665
#include <iostream>
#include <vector>

using namespace std;

void print( vector<int> v ) {
	for( vector<int>::iterator i = v.begin(); i < v.end(); i++ )
		cout << *i << ", ";
	cout << endl;
}

int main() {
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	vector<int>::iterator it = v.begin();
	print( v );

	it += 2;
	it = v.insert(it, 5);
	print( v );

	v.erase(v.begin(), it);
	print( v );

	vector<int> v2;
	v2.push_back(6);
	v2.push_back(7);
	v2.push_back(8);
	v2.push_back(9);

	cout << "vector v: ";
	print( v );
	cout << "vector v2: ";
	print( v2 );

	v.swap( v2 );
	cout << "vector v after swap: ";
	print( v );
	cout << "vector v2 after swap: ";
	print( v2 );

	v.clear();
	cout << "\nvector v after clear: ";
	print( v );


	return 0;
}
