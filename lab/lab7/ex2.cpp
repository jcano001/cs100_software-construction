// Justin Cano 86094560
// Brady Leong 861020665
#include <iostream>
#include <vector>
#include <stack>

using namespace std;

void print( vector<char> v ) {
	for( vector<char>::iterator i = v.begin(); i < v.end(); i++ )
		cout << *i << ", ";
	cout << endl;
}

int main() {
	char input[128];
	cout << "enter characters:\n";
	cin >> input;

	vector<char> v;
	for(int i = 0; input[i] != '\0'; i++)
		v.push_back(input[i]);
	print( v );

	stack< char,vector<char> > s(v);

	while( !s.empty() ) {
		cout << s.top() << ", ";
		s.pop();
	}
	cout << endl;

	return 0;
}
