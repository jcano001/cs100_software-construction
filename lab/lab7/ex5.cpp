// Justin Cano 86094560
// Brady Leong 861020665
#include <iostream>
#include <vector>
#include <map>

using namespace std;

class Phonebook
{
public:
	Phonebook() {}

	void insert(string fullName, int phoneNumber)
	{
		phonebook.insert( pair<string,int>(fullName,phoneNumber) );
	}

	void find( string fullName ) 
	{
		map<string,int>::iterator it = phonebook.find(fullName);
		if( it != phonebook.end() )
			cout << it->second << endl;
		else
			cout << fullName << " not found.\n";
	}

	void deleteEntry( string fullName )
	{
		map<string,int>::iterator it = phonebook.find(fullName);
		if( it != phonebook.end() )
			phonebook.erase(it);
		else
			cout << fullName << " not found.\n";
	}

	void printAll()
	{
		for( map<string,int>::iterator it = phonebook.begin(); it != phonebook.end(); it++ )
		{
			cout << it->first << " => " << it->second << endl;
		}
		cout << endl;
	}
private:
	map<string,int> phonebook;
};

void print( vector<char> v ) {
	for( vector<char>::iterator i = v.begin(); i < v.end(); i++ )
		cout << *i << ", ";
	cout << endl;
}

int main() {
	Phonebook pb;
	pb.insert("justin",123);
	pb.insert("brady",456);

	pb.printAll();

	pb.deleteEntry("justin");
	pb.printAll();

	pb.find("brady");

	return 0;
}
