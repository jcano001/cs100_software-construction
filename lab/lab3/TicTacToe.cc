#include <iostream>

using namespace std;

class Player
{
public:
	Player()
	 :isMyTurn(false), gamePiece('x') {}

	Player(bool turn, char x)
	 :isMyTurn(turn), gamePiece(x) {}

private:
	bool isMyTurn;
	char gamePiece;
};

class TicTacToe
{
public:
	TicTacToe()
	{
		player1 = Player(true,'x');
		player2 = Player(false,'o');
		board = new char[9];
	}
	void print()
	{//was going to 10 not 9.
		for(int i = 0; i < 9; i++)
		{
			cout << board[i] << " ";
			if(i == 2 || i == 5)
				cout << endl;
		}
	}
	void makeTurn(int pos, char x)
	{//made it check for the user attempting to fill in an already used spot.
		if(board[pos] == 'x' || board[pos] == 'o')
		{
			cout << "Invalid Move!\n";
			return;
		}
		board[pos] = x;	
	}

	bool win()
	{//checked for the condion at space 9. doesn't exist.
		if( (board[0] == board[4] && board[0] == board[8] &&( board[0] == 'o' ||board[0] == 'x' ) ))
		{	
			cout << "winner\n";//couts if the game is won.	
			return true;
		}
		
		if(board[0] == board[3] && board[3] == board[6] &&(board[0] == 'o' || board[0] == 'x'))
		{
			cout << "winner\n";
			return true;
		}
		return false;
	}
	//if all the spots are being used then the program should quit.
	bool stop()
	{
		for(int i = 0; i < 9; ++i)
		{
			if(board[i] != 'x' && board[i] != 'o')
				return false;
		}
		cout << "tie\n";//couts if the game was a tie.
				
		return true;
	}

private:
	Player player1;
	Player player2;
	char* board;
};

int main()
{
	TicTacToe t = TicTacToe();
	//conditions were not set.
	bool winner = false;
	bool stopGame = false;
	int turn = 1;	

	// curr = 1 - curr;
	while(!winner && !stopGame){
		int input;

		if(turn == 1)
		{
			cout << "Player 1 enter a space (0-8): ";
			cin >> input;
			t.makeTurn(input,'x');
		}			

		else
		{
			cout << "Player 2 enter space (0-8): ";
			cin >> input;
			t.makeTurn(input,'o');
		}
		t.print();
		winner = t.win();
		stopGame=t.stop();
		//stopGame(above) is never updated. A stalemate is never able to end the program.
		turn = 1 - turn;
	}

	return 0;
}
