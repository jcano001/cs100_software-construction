#include <iostream>

using namespace std;

int main()
{
	int* test  = new int[15];
	for(int i = 0; i < 15; i++) // change loop iteration from i<=16 to i<15
	{
		test[i] = i;
		cout << test[i];
	}

	cout << "TEST 1\n";	
	char sentry = 'a';
	int loop = 0;
	char sentence1[] = "Invalid Char array\0"; // added null characted to terminated char array
	while(sentry != '\0')
	{
		sentry = sentence1[loop];
		cout << sentry; 
		loop++;
	}		

	

	char* sentence = "This is a char array.'\0'"; // added null character to terminate char array
	char* tempHold = sentence;
	//delete[] sentence; no need for this because no new malloc
	
	sentry = 'a';
	loop = 1;
	while(sentry != '\n')
	{
		sentry = sentence[loop];
		cout << sentry;
		loop++;
	}	
	
	int numToRaise;
	int result;
	cout << "\nEnter a number to raise to the power of 5:";
	cin >> numToRaise;
	for(int i = 0; i < 5; ++i)
	{
		result *= numToRaise;
	}
	cout << "Result: " << result << endl;

	cout << "Enter a number whose factorial you want:";
	int factorial;
	int factorialResult = 1; // changed factorialResult from 0 to 1
	cin >> factorial;

	while(factorial > 0)
	{
		
		factorialResult *= factorial;
		factorial--; // factorial was not decrementing before
	}	
	cout << "The factorial is: " << factorialResult << endl;

	int numA;
	int numB;
	cout << "Enter one number and then another one to subtract from it: ";
	cin >> numA >> numB;
	
	cout << "The subtraction is:" << (numA - numB) << endl; //changed from (numB-numA)
	
	
	
	
	return 0;
}
