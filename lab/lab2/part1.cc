// CS100 Lab 2 Part 1
// Justin Cano 860945660
// Nick Lawler 860981873

#include <iostream>

using namespace std;

class dynamicStack
{
public:
	dynamicStack()
	 :capacity(5), size(0)
	{
	  array = new char[capacity];
	  top = array;
	}

	~dynamicStack()
	{
	  delete [] array;
	}

	void print()
	{
		for(int i = 0; i < size; ++i)
		{
			cout << array[i] << " ";
		}
		cout << endl << "Size: " << size << endl << "Capacity: " << capacity << endl;
	}

	void push(char c)
	{
	  
	  if(size+1>capacity) 	
	  {
 	    doubleCapacity();
            size++;
	    top = array + (size-1);
	  // cout << "test1" << endl;
 	  }
         
          else if (size == 0)
	  {
	    size++;
	   // cout << "test2" << endl;
	  }	

	  else if(size+1 <= capacity)
	  {
	    size++;
	    top += 1;
	   // cout << "test3" << endl;
          }	



	*top = c;
	}

	char pop()
	{
		char temp = *top;

		if (size == 0)
	 	{
			cout << "Empty stack" << endl;
			return '\0'; 
	 	}		

		else if (size-1 < 5)
		{
			size--;
			top = array + (size-1);
		}
	
		else if (size-1 <= capacity/2)
		{
			halveCapacity();
			size--;
			top = array + (size-1);
			cout << "test" << endl;
		}
		else
		{
			size--;
			top = array + (size-1);
		}
		
		

		return temp;
	}

private:

	void doubleCapacity()
	{
	  char* temp = new char[capacity*2];
	  capacity = capacity*2;
	  for(int i = 0; i < size; i++)
	  {
	    temp[i] = array[i];
	  }
	  array = temp;
	}

	void halveCapacity()
	{
	  char* temp = new char[capacity/2];
	  capacity = capacity/2;
	  for(int i = 0; i <size; ++i)
	  {
	   temp[i] = array[i];
	  }
	  array = temp;
	}

	char* array;
	int capacity;
	int size;
	char* top;



};


int main()
{
	dynamicStack a;
	
	a.push('a');
	a.push('b');
	a.push('c');
	a.push('d');
	a.push('e');

	a.print();
	cout << endl;
	
	a.push('f');
	a.push('g');
	a.print();
	cout << endl;

	char p = a.pop();
	char q = a.pop();
	char v = a.pop();

	cout << p << " " << q << " " << v << endl;
	a.print();
	cout << endl;








	return 0;
}
