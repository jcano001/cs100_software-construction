// Justin Cano 860945660
// David Ton 861045714
#include <iostream>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <strings.h>
#include <pthread.h>
#include <sys/file.h>

using namespace std;

pthread_mutex_t print_mutex;
pthread_cond_t count_threshold_cv;

#define MY_SOCK_PATH "my_path"
#define BACK_LOG 50
#define MAXDATASIZE 255
const int NUM_THREADS=5;
const char* FILE_NAME = "dictionary.txt";

struct arg_struct {
	int threadid;
	int sockfd;
	int clientfd;
};

struct client_arg_struct {
	int clientfd;
	int numbytes;
	int client_buf[MAXDATASIZE];
};

void *doThreadWork(void *arguments)
{
	struct arg_struct *args = (struct arg_struct *)arguments;
	int sockfd = args->sockfd;
	int clientfd = args->clientfd;
	int threadid = args->threadid;

	int buf[MAXDATASIZE];

	struct sockaddr_un cli_addr;
	socklen_t cli_addr_size = sizeof(struct sockaddr_un);
	
	long tid = (long)threadid;
	cout << "Thread " << tid << " locking....\n";
	pthread_mutex_lock(&print_mutex);
	cout << "Thread " << tid << " waiting for request...\n";
	clientfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cli_addr_size);
	if( clientfd == -1 )
		cout << "Accept Error\n";
	else
		cout << "received request";


	int fd = open(FILE_NAME,O_WRONLY,0666);
	if( fd == -1 )
		cout << "server open failed\n";

	if( read(fd,buf,MAXDATASIZE-1) == -1 )
		cout << "Read Error\n";
	if( write(clientfd,buf,sizeof(buf)) == -1 )
		cout << "Write Error\n";
	sleep(1);
	if( read(client,buf,MAXDATASIZE-1;) == -1 )
		cout << "Read Error\n";

	pthread_mutex_unlock(&print_mutex);
	close(clientfd);
	close(sockfd);
	pthread_exit(NULL);
}


int main( int argc, char *argv[] )
{
	int pid;
//	char sock_path[] = "/my_path/";


	/* Server Side Variables */
	int sockfd;
	int clientfd;
	int size = 0;

	struct sockaddr_un serv_addr, cli_addr;
	socklen_t cli_addr_size;


			bzero((int*)&serv_addr,sizeof(struct sockaddr_un));
			sockfd = socket( AF_UNIX, SOCK_STREAM, 0 );
			serv_addr.sun_family = AF_UNIX;
			strncpy(serv_addr.sun_path,MY_SOCK_PATH,sizeof(serv_addr.sun_path)-1);
			size = sizeof(serv_addr);
			if( bind(sockfd, (struct sockaddr*) &serv_addr, size) == -1 )
				cout << "Bind Error\n";

			if( listen(sockfd, BACK_LOG) == -1 )
				cout << "Listen Error\n";
			unlink(MY_SOCK_PATH);

			cli_addr_size = sizeof(struct sockaddr_un);

			
			pthread_t threads[NUM_THREADS];
			/* Initialize the mutex */
			pthread_mutex_init(&print_mutex,NULL);

			pthread_mutex_lock(&print_mutex);
/*long t = 0;
while( 1 )
{
	clientfd = accept(sockfd,(struct sockaddr *) &cli_addr,&cli_addr_size);
	if( clientfd == -1 )
	{
		cout << "Accept Error\n";
		exit(1);
	}
	
	cout << "connection received";

				struct arg_struct args;
				args.threadid = t;
				args.sockfd = sockfd;
				args.clientfd = clientfd;

				cout << "Creating thread " << t << endl;
				if( int rc = pthread_create(&threads[t],NULL,doThreadWork,(void *)&args))
				{
					cerr << "ERROR; return code from pthread_create() is " << rc << endl;
					exit(-1);
				}

	t++;
}*/
			struct arg_struct args[NUM_THREADS];

			for( long t = 0; t<NUM_THREADS; t++ )
			{
				args[t].threadid = t;
				args[t].sockfd = sockfd;
				args[t].clientfd = clientfd;

				cout << "Creating thread " << t << endl;
				if( int rc = pthread_create(&threads[t],NULL,doThreadWork,(void *)&args[t]))
				{
					cerr << "ERROR; return code from pthread_create() is " << rc << endl;
					exit(-1);
				}
				//pthread_join(threads[t],NULL);
			}
			for( long t = 0; t<NUM_THREADS; t++ )
			{
				pthread_join(threads[t],NULL);
			}


			pthread_mutex_unlock(&print_mutex);
	
	cout << "server exiting\n";

	return 0;
}
