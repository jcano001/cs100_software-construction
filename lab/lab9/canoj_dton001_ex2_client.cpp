// Justin Cano 860945660
// David Ton 861045714
#include <iostream>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <strings.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/file.h>

using namespace std;

pthread_mutex_t print_mutex;

#define MY_SOCK_PATH "my_path"
#define BACK_LOG 50
#define MAXDATASIZE 255
const int NUM_THREADS=5;
const char* FILE_NAME = "dictionary.txt";

struct arg_struct {
	int threadid;
	int sockfd;
	int clientfd;
};

struct client_arg_struct {
	int clientfd;
	int numbytes;
	int client_buf[MAXDATASIZE];
};

int main( int argc, char *argv[] )
{
	int pid;
//	char sock_path[] = "/my_path/";

	int j = atoi(argv[1]);

	/* Server Side Variables */
	int sockfd;

	struct sockaddr_un serv_addr, cli_addr;
	socklen_t cli_addr_size;


	/* Client Side Variables */
	int clientfd, fd;
	int numbytes;
	int client_buf[MAXDATASIZE];

	int size = 0;
			sleep(1);
			bzero((int*)&cli_addr,sizeof(struct sockaddr_un));
			clientfd = socket( AF_UNIX, SOCK_STREAM, 0 );
			cli_addr.sun_family = AF_UNIX;
			strncpy(cli_addr.sun_path,MY_SOCK_PATH,sizeof(cli_addr.sun_path)-1);
			size = sizeof(cli_addr);
			unlink(MY_SOCK_PATH);
			if( connect(clientfd,(struct sockaddr*) &cli_addr,size) == -1 )
				cout << "Connect Error\n";

			fd = open(FILE_NAME,O_WRONLY,0666);
			if( fd == -1 )
				cout << "client open file failed\n";

			numbytes = read(clientfd,client_buf,MAXDATASIZE-1);
			if( numbytes == -1 )
			{
				cout << "Read Error\n";
			//	exit(1);
			}
			sleep(1);

			if( write(fd,client_buf,sizeof(client_buf)) == -1 )
			{
				cout << "client-write() error\n";
			}

			for( int i = 0; i < 5; i++ )
			{
				numbytes = read(clientfd,client_buf,MAXDATASIZE-1);
				if( numbytes == -1 )
				{
					cout << "Read Error\n";
				//	exit(1);
				}
				else
				{
					cout << "Read OK...\n";
					cout << (*client_buf) << " from client " << j << endl;
				}
				sleep(1);
			}

			close(clientfd);


	
	return 0;
}
